package com.generation_change.mhooda.locchatapp.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.global.AppSession;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by anonymous on 12/25/15.
 */
public class StartActivity extends AppCompatActivity {



    private AppSession session;
    private ProgressBar mProgressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        mProgressBar = (ProgressBar)findViewById(R.id.start_progress_bar);
        session = AppSession.get(getApplicationContext());
        mProgressBar.setVisibility(View.VISIBLE);






        if(session.isLoggedIn()){


            new Timer().schedule(new TimerTask() {
                public void run() {
                    StartActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            startActivity(new Intent(StartActivity.this, MainActivity.class));
                        }
                    });
                }
            }, 2000);


        }else{

            new Timer().schedule(new TimerTask() {
                public void run() {
                    StartActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            startActivity(new Intent(StartActivity.this, LoginActivity.class));
                        }
                    });
                }
            }, 2000);

        }

    }

}