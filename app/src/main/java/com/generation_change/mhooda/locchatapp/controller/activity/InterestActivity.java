package com.generation_change.mhooda.locchatapp.controller.activity;

//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.fragment.InterestFragment;
import com.generation_change.mhooda.locchatapp.model.Group;

import java.util.ArrayList;

/**
 * Created by manjeet on 9/12/15.
 */
public class InterestActivity extends AppCompatActivity {
    private String Title;
    private ArrayList<Group> mGroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interest_activity);

        getListfromParent();
        setupToolbar();
        initiate_fragment();


    }

    private void initiate_fragment(){

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.interest_fragment_container);
        if (fragment == null) {
            fragment = new InterestFragment();
            fm.beginTransaction()
                    .add(R.id.interest_fragment_container, fragment)
                    .commit();

        }

    }

    private void setupToolbar(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.interest_toolbar);
        toolbar.setTitle(Title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //toolbar.setBackgroundColor(Color.TRANSPARENT);
    }

    public void getListfromParent(){

        Bundle list = getIntent().getExtras();
        mGroups = new ArrayList<>();
        mGroups = list.getParcelableArrayList("LIST");
        Title = list.getString("Group");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    //giving list to fragment
    public ArrayList<Group> getSharedList(){
        return mGroups;
    }

}