package com.generation_change.mhooda.locchatapp.controller.activity;

/**
 * Created by anonymous on 12/19/15.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.fragment.FacebookFragment;
import com.generation_change.mhooda.locchatapp.controller.fragment.GPlusFragment;
import com.generation_change.mhooda.locchatapp.global.AppSession;


public class LoginActivity extends AppCompatActivity {

    private Vibrator vib;
    Animation animShake;

    private Toolbar toolbar;
    private EditText  loginInputEmail, loginInputPassword;
    private TextInputLayout loginInputLayoutEmail, loginInputLayoutPassword;
    private Button btnlogin;
    private Button btnLinkSignup;
    private Button skipLogin;
    // Session Manager Class
    AppSession session;


   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        session = AppSession.get(getApplicationContext());
        loginInputLayoutEmail = (TextInputLayout) findViewById(R.id.login_input_layout_email);
        loginInputLayoutPassword = (TextInputLayout) findViewById(R.id.login_input_layout_password);
        loginInputEmail = (EditText) findViewById(R.id.login_input_email);
        loginInputPassword = (EditText) findViewById(R.id.login_input_password);
        btnlogin = (Button) findViewById(R.id.btn_login);
        btnLinkSignup = (Button) findViewById(R.id.btn_link_signup);
        skipLogin = (Button)findViewById(R.id.btn_skip_login);

        loginInputEmail.addTextChangedListener(new MyTextWatcher(loginInputEmail));
        loginInputPassword.addTextChangedListener(new MyTextWatcher(loginInputPassword));
        animShake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);




       btnlogin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               submitForm();
           }
       });


       btnLinkSignup.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent i = new Intent(getApplicationContext(),SignupActivity.class);
               startActivity(i);

           }
       });


       skipLogin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent i = new Intent(getApplicationContext(),MainActivity.class);
               startActivity(i);

           }
       });


       //Facebook Fragment
       FragmentManager fm = getSupportFragmentManager();
       Fragment fragment = fm.findFragmentById(R.id.facebook_login_container);


       if (fragment == null) {
           fragment = new FacebookFragment();
           fm.beginTransaction()
                   .add(R.id.facebook_login_container, fragment)
                   .commit();


       }

       //GPlus Fragment
       FragmentManager fm1 = getSupportFragmentManager();
       Fragment fragment1 = fm1.findFragmentById(R.id.gplus_login_container);


       if (fragment1 == null) {
           fragment1 = new GPlusFragment();
           fm1.beginTransaction()
                   .add(R.id.gplus_login_container, fragment1)
                   .commit();


       }




   }

    /**
     * Validating form
     */
    private void submitForm() {

        if (!validateEmail()) {

            loginInputEmail.setAnimation(animShake);
            loginInputEmail.startAnimation(animShake);
            vib.vibrate(120);
            return;
        }

        if (!validatePassword()) {

            loginInputPassword.setAnimation(animShake);
            loginInputPassword.startAnimation(animShake);
            vib.vibrate(120);

            return;
        }

        loginInputLayoutEmail.setErrorEnabled(false);
        loginInputLayoutPassword.setErrorEnabled(false);

        session.createLoginSession("name", loginInputEmail.getText().toString(), loginInputPassword.getText().toString(), "nourl");
        Toast.makeText(getApplicationContext(), "Logging In...!!", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);

    }



    private boolean validateEmail() {
        String email = loginInputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {

            if (loginInputLayoutEmail.getChildCount() == 2)
                loginInputLayoutEmail.getChildAt(1).setVisibility(View.VISIBLE);
            loginInputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(loginInputEmail);
            return false;
        } else {
            loginInputLayoutEmail.setError(null);

            if (loginInputLayoutEmail.getChildCount() == 2)
                loginInputLayoutEmail.getChildAt(1).setVisibility(View.GONE);
        }

        return true;
    }



    private boolean validatePassword() {
        if (loginInputPassword.getText().toString().trim().isEmpty()) {

            if (loginInputLayoutPassword.getChildCount() == 2)
                loginInputLayoutPassword.getChildAt(1).setVisibility(View.VISIBLE);

            loginInputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(loginInputPassword);
            return false;
        } else {

            loginInputLayoutPassword.setError(null);

            if (loginInputLayoutPassword.getChildCount() == 2)
                loginInputLayoutPassword.getChildAt(1).setVisibility(View.GONE);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.login_input_email:
                    validateEmail();
                    break;
                case R.id.login_input_password:
                    validatePassword();
                    break;
            }
        }



    }


}
