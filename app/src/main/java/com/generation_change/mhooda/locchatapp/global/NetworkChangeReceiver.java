package com.generation_change.mhooda.locchatapp.global;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by varun on 20-11-2015.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();
        if (isConnected) {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.CONNECTED);
            Log.i("NET", "connecte" + isConnected);
        } else {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
            Log.i("NET", "not connecte" +isConnected);
        }
    }
}
