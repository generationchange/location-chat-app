package com.generation_change.mhooda.locchatapp.global;

/**
 * Created by anonymous on 12/29/15.
 */
public class User {

    private String mUID;
    private String mName;
    private String mEmail;
    private String mPassword;
    private String mPhotoURL;

    public String getUID() {
        return mUID;
    }

    public void setUID(String mUID) {
        this.mUID = mUID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getPhotoURL() {
        return mPhotoURL;
    }

    public void setPhotoURL(String mPhotoURL) {
        this.mPhotoURL = mPhotoURL;
    }
}
