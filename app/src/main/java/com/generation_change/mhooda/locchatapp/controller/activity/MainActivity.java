package com.generation_change.mhooda.locchatapp.controller.activity;

//import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.generation_change.mhooda.locchatapp.controller.adapter.PagerAdapter;
import com.generation_change.mhooda.locchatapp.controller.fragment.ChatFragment;

import com.generation_change.mhooda.locchatapp.controller.service.ChatService;
import com.generation_change.mhooda.locchatapp.global.AppSession;
import com.generation_change.mhooda.locchatapp.global.GlobalDataContainer;
import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.global.GlobalDataStructures;
import com.generation_change.mhooda.locchatapp.global.ImageHelper;
import com.generation_change.mhooda.locchatapp.global.LruBitmapCache;
import com.generation_change.mhooda.locchatapp.global.ServerCredentials;
import com.generation_change.mhooda.locchatapp.model.Message;
import com.generation_change.mhooda.locchatapp.model.db.DBHelper;
import com.generation_change.mhooda.locchatapp.model.db.MessageDataSource;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {


//    //**** SideBar Code ***///
//
//    private static String TAG = MainActivity.class.getSimpleName();
//
//     ListView mDrawerList;
//     RelativeLayout mDrawerPane;
//     private ActionBarDrawerToggle mDrawerToggle;
//     private DrawerLayout mDrawerLayout;
//
//     ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
//     class NavItem {
//         String mTitle;
//         String mSubtitle;
//         int mIcon;
//
//         public NavItem(String title, String subtitle, int icon) {
//         mTitle = title;
//         mSubtitle = subtitle;
//         mIcon = icon;
//         }
//     }
//
//
//    class DrawerListAdapter extends BaseAdapter {
//
//        Context mContext;
//        ArrayList<NavItem> mNavItems;
//
//        public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
//            mContext = context;
//            mNavItems = navItems;
//        }
//
//        @Override
//        public int getCount() {
//            return mNavItems.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return mNavItems.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return 0;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            View view;
//
//            if (convertView == null) {
//                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                view = inflater.inflate(R.layout.drawer_item, null);
//            }
//            else {
//                view = convertView;
//            }
//
//
//            TextView titleView = (TextView) view.findViewById(R.id.title);
//            TextView subtitleView = (TextView) view.findViewById(R.id.subTitle);
//            ImageView iconView = (ImageView) view.findViewById(R.id.icon);
//
//            titleView.setText( mNavItems.get(position).mTitle );
//            subtitleView.setText( mNavItems.get(position).mSubtitle );
//            iconView.setImageResource(mNavItems.get(position).mIcon);
//
//            return view;
//        }
//    }
//
//     private void selectItemFromDrawer(int position) {
//
////     Add code here if you want to call any fragment on Item Selection
////     Fragment fragment = new PreferencesFragment();
////
////     FragmentManager fragmentManager = getFragmentManager();
////     fragmentManager.beginTransaction()
////     .replace(R.id.mainContent, fragment)
////     .commit();
//         Log.d(TAG,mNavItems.get(position).toString()+" was clicked");
//
//     mDrawerList.setItemChecked(position, true);
//     setTitle(mNavItems.get(position).mTitle);
//     // Close the drawer
//     mDrawerLayout.closeDrawer(mDrawerPane);
//     }
//
//
////*** Side Bar Code Ends Here***///




    private MainActivityBroadcastReceiver mReceiver;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ImageView profilePicImageView;
    private TextView userEmailTextView;
    private TextView userNameTextView;
    private String userProfilePicURL;
    private String userName;
    private String userEmail;
    private AppSession session;

    private FragmentStatePagerAdapter mPagerAdapter;
    final String SWITCH_TAB= "Chat";


    /**
     * Initialization functions
     */
    private void initBroadcastReceiver() {
        if (mReceiver == null) {
            mReceiver = new MainActivityBroadcastReceiver();
            mReceiver.setContext(this);
        }
    }

    private void attachBroadCastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(GlobalDataContainer.RECEIVE_MSG);
        filter.addAction(GlobalDataContainer.RECEIVE_ROSTER);
        filter.addAction(GlobalDataContainer.CONNECTION_STATUS_CHANGE);
        filter.addAction(GlobalDataContainer.RECEIVER_CREDENTIALS);
        this.registerReceiver(mReceiver, filter);
    }

    private void detachBroadCastReceiver() {
        this.unregisterReceiver(mReceiver);
    }

    private void setupToolbar () {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void setupTabs() {
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.addTab(mTabLayout.newTab().setText("Group"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Chat"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Trending"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void setupChatService() {
        if (!GlobalDataContainer.isServiceExists()) {
            Intent intent = new Intent(this, ChatService.class);
            this.startService(intent);
            GlobalDataContainer.setServiceCreated(true);
        }
    }

    private void setupPager() {
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new PagerAdapter
                (getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void databaseTestInit() {
        DBHelper dbHelper = DBHelper.getInstance(this);

        SQLiteDatabase sqLiteDatabase = dbHelper.getDB();
        dbHelper.onUpgrade(sqLiteDatabase, 0, 0);

        MessageDataSource messageDataSource = new MessageDataSource(sqLiteDatabase);
        Message message = new Message();

        message.setKeyRemoteJid("admin@xmpp.local");
        message.setData("hello user1!!");
        message.setTimestamp((int) System.currentTimeMillis() / 1000);
        message.setKeyFromMe(0);
        message.setNeedsPush(1);
        messageDataSource.insert(message);

        message.setKeyRemoteJid("admin@xmpp.local");
        message.setData("hello admin!!");
        message.setTimestamp((int) System.currentTimeMillis() / 1000);
        message.setKeyFromMe(1);
        message.setNeedsPush(1);
        messageDataSource.insert(message);

        message.setKeyRemoteJid("group1@xmpp.local");
        message.setData("hello user2 user1!!");
        message.setTimestamp((int) System.currentTimeMillis() / 1000);
        message.setKeyFromMe(0);
        message.setNeedsPush(1);
        message.setUserJid("user1@xmpp.local");
        messageDataSource.insert(message);
    }



    private void setupSideBar() {
//        mNavItems.add(new NavItem("Home", "Meetup destination", R.mipmap.ic_launcher));
//        mNavItems.add(new NavItem("Preferences", "Change your preferences", R.mipmap.ic_launcher));
//        mNavItems.add(new NavItem("About", "Get to know about us", R.mipmap.ic_launcher));
//
//        // DrawerLayout
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//
//        // Populate the Navigation Drawer with options
//        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
//
//
//
//        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
//        // Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
//                R.array.From_users_array, android.R.layout.simple_spinner_item);
//        // Specify the layout to use when the list of choices appears
//        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        // Apply the adapter to the spinner
//        spinner1.setAdapter(adapter1);
//
//        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
//        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
//                R.array.To_users_array, android.R.layout.simple_spinner_item);
//        // Specify the layout to use when the list of choices appears
//        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner2.setAdapter(adapter2);
//
//
//
//        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                // your code here
//                String item = parent.getItemAtPosition(position).toString();
//                if (position != 0) {
//                    // Showing selected spinner item
//                    Toast.makeText(parent.getContext(), "From: " + item, Toast.LENGTH_SHORT).show();
//                    senderCredentialBroadcast(item);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });
//
//        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                // your code here
//                String item = parent.getItemAtPosition(position).toString();
//                if (position != 0) {
//                    // Showing selected spinner item
//
//                    // Showing selected spinner item
//                    Toast.makeText(parent.getContext(), "To: " + item, Toast.LENGTH_SHORT).show();
//                    receiverCredentialBroadcast(item);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });
//
//
//        mDrawerList = (ListView) findViewById(R.id.navList);
//        DrawerListAdapter adapter3 = new DrawerListAdapter(this, mNavItems);
//        mDrawerList.setAdapter(adapter3);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        // Drawer Item click listeners
//        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                selectItemFromDrawer(position);
//            }
//        });
//
//
//        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                Log.d(TAG, "onDrawerOpen: " + getTitle());
//
//                invalidateOptionsMenu();
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//                Log.d(TAG, "onDrawerClosed: " + getTitle());
//
//                invalidateOptionsMenu();
//            }
//
//
//
//        };
//
//        mDrawerLayout.setDrawerListener(mDrawerToggle);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.nav_profile) {
                    // Handle the camera action
                }else if(id == R.id.nav_groups){

                } else if (id == R.id.nav_people) {

                } else if (id == R.id.nav_set_location) {

                } else if (id == R.id.nav_manage_events) {

                } else if (id == R.id.nav_settings) {

                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });


        ImageLoader imageLoader = new ImageLoader(Volley.newRequestQueue(getApplicationContext()),
                new LruBitmapCache());

        profilePicImageView = (ImageView)findViewById(R.id.userProfilePicimageView);
        userEmailTextView = (TextView)findViewById(R.id.userEmailTextView);
        userNameTextView = (TextView)findViewById(R.id.userNameTextView);
        userEmailTextView.setText(session.getUserDetails().get(AppSession.KEY_EMAIL));
        userNameTextView.setText(session.getUserDetails().get(AppSession.KEY_NAME));
        userProfilePicURL = session.getUserDetails().get(AppSession.KEY_URL);
        Toast t = Toast.makeText(getApplicationContext(),""+session.getUserDetails().get(AppSession.KEY_EMAIL)+ session.getUserDetails().get(AppSession.KEY_NAME) + session.getUserDetails().get(AppSession.KEY_URL),Toast.LENGTH_SHORT);
        t.show();


        if(userProfilePicURL != null){


            imageLoader.get(userProfilePicURL, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("MainActivity", "Image Load Error: " + error.getMessage());
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview

                        profilePicImageView.setImageBitmap(ImageHelper.getRoundedCornerBitmap(getApplication(), response.getBitmap(), 250, 200, 200, false, false, false, false));

                    }
                }
            });

        }




    }

    public void senderCredentialBroadcast(String UserName){
        GlobalDataContainer.setUserJIDAndPassword(UserName);
        Intent intent = new Intent(GlobalDataContainer.SENDER_CREDENTIALS);
        this.sendBroadcast(intent);
    }
    public void receiverCredentialBroadcast(String UserName) {
        //GlobalDataContainer.setUserJIDAndPassword(UserName);
        Intent intent = new Intent(GlobalDataContainer.RECEIVER_CREDENTIALS);
        intent.putExtra(GlobalDataContainer.RECEIVER_CREDENTIALS_JID, UserName);
        this.sendBroadcast(intent);
    }

    private void switchtotab(int Tab){
                mViewPager.setCurrentItem(Tab);
        }

    /**
     * Callbacks
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = AppSession.get(getApplicationContext());

        initBroadcastReceiver();
        setContentView(R.layout.activity_main);
        setupToolbar();
        setupTabs();
        setupChatService();
        setupPager();
        setupSideBar();
        setupUserInfoView();
        //for back button
        final Intent intent = getIntent();
        if(intent.hasExtra("Chat")){
              int Tab = intent.getExtras().getInt("Chat");
              switchtotab(Tab);
        }

        databaseTestInit();
    }
    public void setupUserInfoView() {
        if (ServerCredentials.USER != "") {
//            TextView sidebarUserView = (TextView) findViewById(R.id.userName);
//            sidebarUserView.setText(ServerCredentials.USER);
            if (GlobalDataContainer.getConnectionState() == GlobalDataContainer.ConnectionState.CONNECTED) {
//                TextView connectionStateView = (TextView) findViewById(R.id.connection_state);
//                connectionStateView.setText("Connected");
            }
        }
    }
    public void setupReceiverCredentials(String ReceiverJid) {

        mViewPager.setCurrentItem(1);
        ChatFragment chatFrag = (ChatFragment)mPagerAdapter.getItem(1);
        chatFrag.addChat(ReceiverJid);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


//        // Pass the event to ActionBarDrawerToggle
//        // If it returns true, then it has handled
//        // the nav drawer indicator touch event
//        switch (item.getItemId()) {
//            // Respond to the action bar's Up/Home button
//            case android.R.id.home:
//
//                if(mDrawerLayout.isDrawerOpen(mDrawerPane)){
//                    mDrawerLayout.closeDrawer(mDrawerPane);
//
//                }
//                else{
//                    mDrawerLayout.openDrawer(mDrawerPane);
//                }
//                //return true;
//            case R.id.main_activity_new_gp:
//                //Bundle bundle = new Bundle();
//                //bundle.putString("Caller", "New_group");
//                Intent intent = new Intent(this, NewGroupActivity.class);
//                //intent.putExtras(bundle);
//                this.startActivity(intent);
//
//                return true;
//        }
//        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);

    }

//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//        mDrawerToggle.syncState();
//    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupChatService();
        attachBroadCastReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        detachBroadCastReceiver();
    }
}

class MainActivityBroadcastReceiver extends BroadcastReceiver {

    private MainActivity mMainActivity;
    public  void setContext(MainActivity pMainActivity) {
        mMainActivity = pMainActivity;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(GlobalDataContainer.CONNECTION_STATUS_CHANGE)) {
            mMainActivity.setupUserInfoView();
        }
        if (action.equals(GlobalDataContainer.RECEIVER_CREDENTIALS)) {
            String ReceiverJid = intent.getStringExtra(GlobalDataContainer.RECEIVER_CREDENTIALS_JID);
            mMainActivity.setupReceiverCredentials(GlobalDataContainer.getReceiverJid(ReceiverJid));
        }
        if (action.equals(GlobalDataContainer.RECEIVE_MSG)) {
            Message message = intent.getExtras().getParcelable(GlobalDataContainer.MESSAGE_BUNDLE);
            Toast.makeText(mMainActivity, "message received" + message.getKeyRemoteJid() +
                    message.getData(), Toast.LENGTH_LONG).show();
            android.support.v4.content.Loader<Object> loader = mMainActivity.
                                                               getSupportLoaderManager().
                                                               getLoader(ChatFragment.LOADER_ID);
            if (loader != null) {
                loader.onContentChanged();
            }
        }
        if (action.equals(GlobalDataContainer.RECEIVE_ROSTER)) {
            //TODO : Manjeet : This is list of subscribed people received.
            ArrayList<String> roster  = intent.getStringArrayListExtra(GlobalDataContainer.ROSTER_BUNDLE);
            for(String temp : roster) {
            GlobalDataStructures.RosterEntryParser rosterEntryParser = new GlobalDataStructures.RosterEntryParser(temp);
                //TODO : Manjeet call your API from here.
                //TODO : Once you call your api remove this toast.
                //This code here shows the usage . Use it to do your stuff.
                Toast.makeText(mMainActivity, "roster received. User Name : " + rosterEntryParser.getUserName() + " and status is : " + rosterEntryParser.getStatus(), Toast.LENGTH_LONG).show();
            }
        }
    }

}