package com.generation_change.mhooda.locchatapp.controller.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.activity.MessageActivity;
import com.generation_change.mhooda.locchatapp.global.GlobalDataContainer;
import com.generation_change.mhooda.locchatapp.model.Chat;
import com.generation_change.mhooda.locchatapp.model.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manjeet on 5/12/15.
 */
public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.InterestHolder> {
    private ArrayList<Group> mGroupList;
    private Context mContext;

    public static class InterestHolder extends RecyclerView.ViewHolder {//implements View.OnClickListener {
        private TextView mGroupNameTextView;
        private TextView mGroupNumMembers;
        private TextView mGroupDescription;
        private ImageView mGroupIconImageView;
        private Context mContext;
        private Group mGroup;
        private int Photo_id;

        public InterestHolder(View itemView, Context context) {
            super(itemView);
            //itemView.setOnClickListener(this);
            mGroupNameTextView = (TextView) itemView.findViewById(R.id.profile_gp_name);
            mGroupNumMembers = (TextView)itemView.findViewById(R.id.profile_gp_members);
            mGroupIconImageView = (ImageView) itemView.findViewById(R.id.profile_gp_photo);
            mGroupDescription = (TextView)itemView.findViewById(R.id.profile_gp_description);
            mContext = context;
        }

        /*public void draw_round_image() {
            Resources res = mContext.getResources();
            Bitmap src = BitmapFactory.decodeResource(res, Photo_id);
            RoundedBitmapDrawable dr =
                    RoundedBitmapDrawableFactory.create(res, src);
            //dr.setCornerRadius(Math.min(dr.getMinimumWidth(),dr.getMinimumHeight()));
            dr.setCircular(true);
            mGroupIconImageView.setImageDrawable(dr);
        }*/


        public void bindGroup(Group group) {
            mGroup = group;
            mGroupNameTextView.setText(mGroup.getmGpName());
            mGroupNumMembers.setText(mGroup.getmGpNumMem());
            mGroupDescription.setText(mGroup.getmDescription());
            mGroupIconImageView.setImageResource(mGroup.getmPhotoid());
            //Photo_id = mGroup.getmPhotoid();
            //draw_round_image();
        }

        /*@Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, MessageActivity.class);
            intent.putExtra(GlobalDataContainer.CHAT_BUNDLE, mChat);
            mContext.startActivity(intent);
        }*/
    };

    public InterestAdapter(Context context, ArrayList<Group> groups){
        mGroupList = groups;
        mContext = context;
    }

    @Override
    public InterestHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(
                R.layout.profile_gp_layout, parent, false);
        return new InterestHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(InterestHolder holder, int position){
        Group group = mGroupList.get(position);
        holder.bindGroup(group);

    }

    @Override
    public int getItemCount(){
        return mGroupList.size();
    }
}