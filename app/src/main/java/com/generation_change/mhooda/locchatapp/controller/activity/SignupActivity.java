package com.generation_change.mhooda.locchatapp.controller.activity;

/**
 * Created by anonymous on 12/19/15.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.global.AppSession;


public class SignupActivity extends AppCompatActivity {

    private Vibrator vib;
    Animation animShake;

    private Toolbar toolbar;
    private EditText signupInputName, signupInputEmail, signupInputPassword, signupInputDOB;
    private TextInputLayout signupInputLayoutName, signupInputLayoutEmail, signupInputLayoutPassword,signupInputLayoutDOB;
    private Button btnSignUp;
    private Button btnLinkLogin;
    private AppSession session;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        session = AppSession.get(getApplicationContext());



        signupInputLayoutName = (TextInputLayout) findViewById(R.id.signup_input_layout_name);
        signupInputLayoutEmail = (TextInputLayout) findViewById(R.id.signup_input_layout_email);
        signupInputLayoutPassword = (TextInputLayout) findViewById(R.id.signup_input_layout_password);
        signupInputLayoutDOB = (TextInputLayout) findViewById(R.id.signup_input_layout_dob);



        signupInputName = (EditText) findViewById(R.id.signup_input_name);
        signupInputEmail = (EditText) findViewById(R.id.signup_input_email);
        signupInputPassword = (EditText) findViewById(R.id.signup_input_password);
        signupInputDOB = (EditText) findViewById(R.id.signup_input_dob);




        btnSignUp = (Button) findViewById(R.id.btn_signup);
        btnLinkLogin = (Button) findViewById(R.id.btn_link_login);


        signupInputName.addTextChangedListener(new MyTextWatcher(signupInputName));
        signupInputEmail.addTextChangedListener(new MyTextWatcher(signupInputEmail));
        signupInputPassword.addTextChangedListener(new MyTextWatcher(signupInputPassword));
        animShake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        btnLinkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);

            }
        });
    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateName()) {

            signupInputName.setAnimation(animShake);
            signupInputName.startAnimation(animShake);
            vib.vibrate(120);
            return;


        }

        if (!validateEmail()) {

            signupInputEmail.setAnimation(animShake);
            signupInputEmail.startAnimation(animShake);
            vib.vibrate(120);
            return;
        }

        if (!validatePassword()) {

            signupInputPassword.setAnimation(animShake);
            signupInputPassword.startAnimation(animShake);
            vib.vibrate(120);

            return;
        }
        if (!validateDOB()) {

            signupInputDOB.setAnimation(animShake);
            signupInputDOB.startAnimation(animShake);
            vib.vibrate(120);
            return;
        }



        signupInputLayoutName.setErrorEnabled(false);
        signupInputLayoutEmail.setErrorEnabled(false);
        signupInputLayoutPassword.setErrorEnabled(false);
        signupInputLayoutDOB.setErrorEnabled(false);


        session.createLoginSession("name", signupInputName.getText().toString(), signupInputPassword.getText().toString(), "nourl");
        Toast.makeText(getApplicationContext(), "Logging In...!!", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);    }

    private boolean validateName() {
        if (signupInputName.getText().toString().trim().isEmpty()) {

            if (signupInputLayoutName.getChildCount() == 2)
                signupInputLayoutName.getChildAt(1).setVisibility(View.VISIBLE);

            signupInputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(signupInputName);
            return false;
        } else {
            signupInputLayoutName.setError(null);

            if (signupInputLayoutName.getChildCount() == 2)
                signupInputLayoutName.getChildAt(1).setVisibility(View.GONE);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = signupInputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {

            if (signupInputLayoutEmail.getChildCount() == 2)
                signupInputLayoutEmail.getChildAt(1).setVisibility(View.VISIBLE);
            signupInputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(signupInputEmail);
            return false;
        } else {
            signupInputLayoutEmail.setError(null);

            if (signupInputLayoutEmail.getChildCount() == 2)
                signupInputLayoutEmail.getChildAt(1).setVisibility(View.GONE);
        }

        return true;
    }



    private boolean validatePassword() {
        if (signupInputPassword.getText().toString().trim().isEmpty()) {

            if (signupInputLayoutPassword.getChildCount() == 2)
                signupInputLayoutPassword.getChildAt(1).setVisibility(View.VISIBLE);

            signupInputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(signupInputPassword);
            return false;
        } else {

            signupInputLayoutPassword.setError(null);

            if (signupInputLayoutPassword.getChildCount() == 2)
                signupInputLayoutPassword.getChildAt(1).setVisibility(View.GONE);
        }

        return true;
    }

    private boolean validateDOB() {

        boolean isDateValid = false;
        String[] s = signupInputDOB.getText().toString().split("/");
        int date = Integer.parseInt(s[0]);
        int month = Integer.parseInt(s[1]);

        if(date <32 && month <13)
            isDateValid = true;

        if (signupInputName.getText().toString().trim().isEmpty()  && isDateValid) {

            if (signupInputLayoutName.getChildCount() == 2)
                signupInputLayoutName.getChildAt(1).setVisibility(View.VISIBLE);

            signupInputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(signupInputName);
            return false;
        } else {
            signupInputLayoutName.setError(null);

            if (signupInputLayoutName.getChildCount() == 2)
                signupInputLayoutName.getChildAt(1).setVisibility(View.GONE);
        }

        return true;
    }





    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.signup_input_name:
                    validateName();
                    break;
                case R.id.signup_input_email:
                    validateEmail();
                    break;
                case R.id.signup_input_password:
                    validatePassword();
                    break;
                case R.id.signup_input_dob:
                    validateDOB();
                    break;
            }
        }



    }


}
