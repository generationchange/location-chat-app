package com.generation_change.mhooda.locchatapp.global;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.generation_change.mhooda.locchatapp.controller.service.ChatService;

/**
 * Created by varun on 23-11-2015.
 */
public class BootMessageReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent_pass)
    {
        if (!GlobalDataContainer.isServiceExists()) {
            Intent intent = new Intent(context, ChatService.class);
            context.startService(intent);
            //Log.i("Autostart", "started");
        }
    }
}
