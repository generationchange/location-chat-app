package com.generation_change.mhooda.locchatapp.controller.adapter;

/**
 * Created by mhooda on 10/6/2015.
 */
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.generation_change.mhooda.locchatapp.controller.fragment.ChatFragment;
import com.generation_change.mhooda.locchatapp.controller.fragment.GroupFragment;
import com.generation_change.mhooda.locchatapp.controller.fragment.TrendingFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private ChatFragment _m_ChatFragment = null;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                GroupFragment groupFragment = new GroupFragment();
                return groupFragment;
            case 1:
                if (_m_ChatFragment == null) {
                    _m_ChatFragment = new ChatFragment();
                }
                return _m_ChatFragment;
            case 2:
                TrendingFragment trendingFragment = new TrendingFragment();
                return trendingFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}