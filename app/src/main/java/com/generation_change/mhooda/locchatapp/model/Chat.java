package com.generation_change.mhooda.locchatapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by himt18 on 22/10/15.
 * Builder class
 */
public class Chat implements Parcelable{
    private int id;
    private String mKeyRemoteJid;
    private int mMessageTableId;
    private int mSortTimestamp;

    public int getId() {
        return id;
    }

    // id not to be set when inserting or updating chat
    public Chat setId(int id) {
        this.id = id;
        return this;
    }

    public int getMessageTableId() {
        return mMessageTableId;
    }

    public Chat setMessageTableId(int mMessageTableId) {
        this.mMessageTableId = mMessageTableId;
        return this;
    }


    public int getmSortTimestamp() {
        return mSortTimestamp;
    }

    public Chat setmSortTimestamp(int mSortTimestamp) {
        this.mSortTimestamp = mSortTimestamp;
        return this;
    }

    public String getKeyRemoteJid() {
        return mKeyRemoteJid;
    }

    public Chat setKeyRemoteJid(String keyRemoteJid) {
        this.mKeyRemoteJid = keyRemoteJid;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(id);
        out.writeString(mKeyRemoteJid);
        out.writeInt(mMessageTableId);
        out.writeInt(mSortTimestamp);
    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel parcel) {
            return new Chat(parcel);
        }

        @Override
        public Chat[] newArray(int i) {
            return new Chat[i];
        }
    };

    private Chat (Parcel in) {
        id = in.readInt();
        mKeyRemoteJid = in.readString();
        mMessageTableId = in.readInt();
        mSortTimestamp = in.readInt();
    }

    public Chat () {}
}
