package com.generation_change.mhooda.locchatapp.controller.activity;


import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

//import android.support.annotation

import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.generation_change.mhooda.locchatapp.controller.adapter.MessageAdapter;
import com.generation_change.mhooda.locchatapp.global.GlobalDataContainer;
import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.model.Chat;
import com.generation_change.mhooda.locchatapp.model.Message;
import com.generation_change.mhooda.locchatapp.model.db.DBHelper;
import com.generation_change.mhooda.locchatapp.model.db.MessageDataSource;
import com.generation_change.mhooda.locchatapp.model.db.SQLiteDataLoader;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.io.File;

/**
 * Created by mhooda on 10/4/2015.
 */
public class MessageActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Message> > {

    private ArrayList<Message> mMessages = new ArrayList<>();
    private RecyclerView mMessagesView;
    private MessageAdapter mMessagesAdapter;

    public File storageDir;
    private int screen_height;
    private int screen_width;

    private EditText mSendText;
    private Button mSendButton;
    private String mMsgText;
    private ImageView camera_button;
    private static final int REQUEST_CAMERA= 1;
    private File mFile;
    private Message file_message;
    private String fileMessageId;
    private String mFilePath;
    private Dialog dialog;

    private Context mContext;
    private Chat mChat;
    private String LASTSEEN = "last seen 1 min ago";
    private String ContactPhoto = "mine";

    private boolean group; //check whether chat is a group chat or not
    private boolean image = false; //check whether msg is img
    private String mGpMembers = "100 Members";


    private MessageActivityBroadcastReceiver mReceiver;

    private final int LOADER_ID = 1;

    // For reverse layout
    private boolean reverse_layout = false;
    private String rv_layout = "Enable Reverse Layout";

    /**
     * Activity callbacks
     */
    @Override
    public void onResume() {
        super.onResume();
        attachBroadCastReceiver();
    }

    @Override
    public void onPause() {
        detachBroadCastReceiver();
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_msg_view);
        mContext = this;
        dialog=new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        getScreenDimensions();
        initBroadCastReceiver();    // BroadCast receiver for received message
        mChat = getChatFromIntent(); // Receive intent from MainActivity
        setupMessagesViewAndAdapter();        // Setup adapter
        setupToolbar();             // Setup toolbar title
        setupSendTextView();        // Setup Send TextView Listener
        setupSendButton();          // Setup Send Button Listener
        setupSQLiteDataLoader();    // Setup Loader
        setupToolbarListener();
        setupCameraListener();
        setupImageViewListener();
        mMessagesView.scrollToPosition(mMessages.size() - 1);

    }

    public void setupImageViewListener(){
        if((ImageView)findViewById(R.id.chat_message_send_image) != null) {
            final ImageView imageView = (ImageView) findViewById(R.id.chat_message_send_image);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(screen_width, screen_height);
                    imageView.setLayoutParams(layoutParams);
                    imageView.setImageBitmap(imageView.getDrawingCache());

                }
            });
        }
    }

    public void getScreenDimensions(){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screen_height = displaymetrics.heightPixels;
        screen_width = displaymetrics.widthPixels;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_msg_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.message_activity_new_gp:
                Intent intent = new Intent(this, NewGroupActivity.class);
                this.startActivity(intent);
                return true;

            case R.id.reverse_layout_enabler:
                if(!reverse_layout){
                    reverse_layout = true;
                    rv_layout = "Disable Reverse Layout";
                    setupMessagesViewAndAdapter();        // Setup adapter
                    invalidateOptionsMenu();

                }
                else{
                    reverse_layout = false;
                    rv_layout = "Enable Reverse Layout";
                    //item.setTitle("Enable Interest Scroll");
                    setupMessagesViewAndAdapter();        // Setup adapter
                    invalidateOptionsMenu();

                    //rv_menu.setTitle("Enable Interest Scroll");
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.reverse_layout_enabler).setTitle(rv_layout);
        return super.onPrepareOptionsMenu(menu);
    }


    /**
     * Message receive callback, called in broadcastreceiver
     */
    public void onReceive(Message message){
        if (message.getKeyRemoteJid().equals(mChat.getKeyRemoteJid())) {
            mMessages.add(message);
            mMessagesAdapter.notifyDataSetChanged();
            mMessagesView.scrollToPosition(mMessages.size()-1);
        }
    }

    /**
     * Loader callbacks
     */
    @Override
    public Loader<ArrayList<Message>> onCreateLoader(int i, Bundle bundle) {

        // DBHelper Singleton class
        DBHelper dbHelper = DBHelper.getInstance(this);

        // writable database
        SQLiteDatabase sqLiteDatabase = dbHelper.getDB();

        // MessageDataSource performs all CRUD operations
        MessageDataSource messageDataSource = new MessageDataSource(sqLiteDatabase);

        SQLiteDataLoader<Message> sqLiteDataLoader = new SQLiteDataLoader<>(this,
                messageDataSource, MessageDataSource.COLUMN_KEY_REMOTE_JID + "=?",
                new String[] {mChat.getKeyRemoteJid()}, null, null,
                MessageDataSource.COLUMN_TIMESTAMP);
        return sqLiteDataLoader;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Message>> arrayListLoader,
                               ArrayList<Message> messages) {
        mMessages.clear();
        for(Message message:messages) {
            mMessages.add(message);
        }
        mMessagesAdapter.notifyDataSetChanged();
        mMessagesView.scrollToPosition(mMessages.size() - 1);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Message>> arrayListLoader) {
        mMessages.clear();
        mMessagesAdapter.notifyDataSetChanged();
        mMessagesView.scrollToPosition(mMessages.size() - 1);
    }


    /**
     * Initialization functions
     */
    private void initBroadCastReceiver() {
        if (mReceiver == null) {
            mReceiver = new MessageActivityBroadcastReceiver();
            mReceiver.setContext(this);
        }
    }

    private void attachBroadCastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(GlobalDataContainer.RECEIVE_MSG);
        //filter.addAction(GlobalDataContainer.MESSAGE_3);
        this.registerReceiver(mReceiver, filter);
    }

    private void detachBroadCastReceiver() {
        this.unregisterReceiver(mReceiver);
    }

    private Chat getChatFromIntent() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Chat chat = bundle.getParcelable(GlobalDataContainer.CHAT_BUNDLE);
        if(intent.hasExtra("group")) {
            group = true;
            ContactPhoto = bundle.getString("gpPhoto");
            LASTSEEN = mGpMembers;
        }
        else
            group = false;
        return chat;
    }

    // camera code starts
    private void setupCameraListener(){
        camera_button = (ImageView)findViewById(R.id.chat_message_camera_button);
        camera_button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                mFile = null;
                try {
                    mFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    //Toast.makeText(mContext,"File not created",Toast.LENGTH_SHORT).show();
                }
                // Continue only if the File was successfully created
                if (mFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(mFile));
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            }
        }
        });
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        file_message = new Message();
        fileMessageId = Integer.toString(file_message.getId());

        String imageFileName = "JPEG_" + fileMessageId + "_";
        storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mFilePath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mFilePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public void compress_image(){
        //compress image before saving
        Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath());
        FileOutputStream out = null;
        String filename = mFile.getAbsolutePath();
        try {
            out = new FileOutputStream(filename);

            //write the compressed bitmap at the destination specified by filename.
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
                compress_image();
                file_message.setData(mFile.getAbsolutePath());
                file_message.setFile(1);
                file_message.setKeyFromMe(1);
                file_message.setKeyRemoteJid(mChat.getKeyRemoteJid());
                file_message.setTimestamp((int) System.currentTimeMillis() / 1000);
                file_message.setNeedsPush(1);
                addItems(file_message);
            }
    }


    private void setupMessagesViewAndAdapter() {
        mMessagesView = (RecyclerView)findViewById(R.id.message_recycler_view);
        mMessagesAdapter = new MessageAdapter(mContext, mMessages);
        mMessagesAdapter.getDataFromActivity(group, image, screen_width);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setReverseLayout(reverse_layout);
        mLayoutManager.setStackFromEnd(reverse_layout);
        mMessagesView.setLayoutManager(mLayoutManager);
        mMessagesView.setAdapter(mMessagesAdapter);
    }

    private void setupToolbar() {
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.chatbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView chat_name = (TextView)findViewById(R.id.chat_contact_name);
        TextView last_seen = (TextView)findViewById(R.id.last_seen);

        chat_name.setText(mChat.getKeyRemoteJid());
        last_seen.setText(LASTSEEN);
        draw_round_image();

    }

    //used by back button
    @Override
    public Intent getSupportParentActivityIntent(){
        final Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Chat", 1);
        return intent;
    }

    //toolbar listener
    public void setupToolbarListener() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.chat_details);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, com.generation_change.mhooda.locchatapp.controller.activity.ChatContactInfo.class);
                //intent.putExtra(GlobalDataContainer.CHAT_BUNDLE, mChat);
                intent.putExtra("NAME", mChat.getKeyRemoteJid());
                intent.putExtra("LASTSEEN", LASTSEEN);
                intent.putExtra("PHOTO", ContactPhoto);
                mContext.startActivity(intent);
            }
        });
    }

    public static int getImageId(Context context, String imageName) {
               return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    public void draw_round_image() {
        Resources res = getResources();
        Bitmap src = BitmapFactory.decodeResource(res, getImageId(this, ContactPhoto));
        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(res, src);
        //dr.setCornerRadius(Math.min(dr.getMinimumWidth(),dr.getMinimumHeight()));
        dr.setCircular(true);
        ImageView imageView = (ImageView)findViewById(R.id.conversation_contact_photo);
        imageView.setImageDrawable(dr);
    }

    private void setupSendTextView() {
        mSendText = (EditText) findViewById(R.id.edit_message);
        mSendText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                mMessagesView.scrollToPosition(mMessages.size() - 1);

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mMsgText = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setupSendButton() {
        mSendButton = (Button)findViewById(R.id.send_msg);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = new Message();
                message.setData(mMsgText);
                message.setKeyFromMe(1);
                message.setKeyRemoteJid(mChat.getKeyRemoteJid());
                message.setTimestamp((int) System.currentTimeMillis() / 1000);
                message.setNeedsPush(1);

                addItems(message);
                Intent intent = new Intent(GlobalDataContainer.SEND_MESSAGE);
                intent.putExtra(GlobalDataContainer.MESSAGE_BUNDLE, message);
                intent.setPackage(mContext.getPackageName());
                mContext.sendBroadcast(intent);

            }

        });

    }

    private void setupSQLiteDataLoader() {
        // Initialize loader with loader id specified. Loader is to perform operations on separate
        // thread
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);
    }

    /**
     * Add items to Main List View and DB
     */
    public void addItems(Message message) {
        mMessages.add(message);
        mMessagesAdapter.notifyDataSetChanged();
        mMessagesView.scrollToPosition(mMessages.size()-1);
        mSendText.setText(null);
    }


}

class MessageActivityBroadcastReceiver extends BroadcastReceiver {

    private MessageActivity mMsgActivity;
    public  void setContext(MessageActivity pContext) {
        mMsgActivity = pContext;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(GlobalDataContainer.RECEIVE_MSG)) {
            Message message = intent.getExtras().getParcelable(GlobalDataContainer.MESSAGE_BUNDLE);
            //TODO : Manjeet  : This is a message received from server. Call message activity API to update message from here.
            Toast.makeText(mMsgActivity, "message received from : " + message.getKeyRemoteJid() +
                    " and message is  : " + message.getData(), Toast.LENGTH_LONG).show();
            //TODO : backend  : This message activity API is to update message from here and expect string msg
            mMsgActivity.onReceive(message);
        }
    }

}