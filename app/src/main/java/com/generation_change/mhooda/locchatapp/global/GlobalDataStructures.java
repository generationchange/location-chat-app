package com.generation_change.mhooda.locchatapp.global;

import java.util.StringTokenizer;

/**
 * Created by varun on 01-11-2015.
 */
public class GlobalDataStructures {
    static public class RosterEntryParser {
        private String mUserName;
        private String mStatus;
        public RosterEntryParser(String rosterEntry){
            StringTokenizer strToken = new StringTokenizer(rosterEntry , ":");
            mUserName = strToken.nextToken(":");
            mStatus = strToken.nextToken(":");

        }
        public String getUserName(){ return mUserName;}
        public String getStatus(){return mStatus;}
    }
}
