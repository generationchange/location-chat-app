package com.generation_change.mhooda.locchatapp.model.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.generation_change.mhooda.locchatapp.model.Chat;
import com.generation_change.mhooda.locchatapp.model.Message;

import java.util.ArrayList;

/**
 * Created by himt18 on 08/11/15.
 */
public class MessageDataSource extends DataSource<Message> {

    public static final String TABLE_NAME = "messages";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_KEY_REMOTE_JID = "key_remote_jid";
    public static final String COLUMN_USER_JID = "user_jid";
    public static final String COLUMN_KEY_FROM_ME = "key_from_me";
    public static final String COLUMN_NEEDS_PUSH = "needs_push";
    public static final String COLUMN_DATA = "data";
    public static final String COLUMN_FILE = "file";
    public static final String COLUMN_TIMESTAMP = "timestamp";


    public MessageDataSource(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
    }

    @Override
    public boolean insert (Message entity) {
        // write to db

        if (entity == null) {
            return false;
        }
        int id = (int) mSQLiteDatabase.insert(TABLE_NAME, null, generateContentValuesFromObject(entity));
        if (id == -1) {
            try { throw new Exception("failed to insert message"); } catch (Exception e) { }
        }
        Chat chat = new Chat();
        chat.setKeyRemoteJid(entity.getKeyRemoteJid());
        chat.setmSortTimestamp(entity.getTimestamp());
        chat.setMessageTableId(id);

        ChatDataSource chatDataSource = new ChatDataSource(mSQLiteDatabase);
        if (!chatDataSource.update(chat)) {
            if (!chatDataSource.insert(chat)) {
                try { throw new Exception("failed to insert chat"); } catch (Exception e) { }
            }
        }
        return true;
    }

    @Override
    public ArrayList<Message> read() {
        ArrayList<Message> messageList = new ArrayList<>();
        Cursor cursor = mSQLiteDatabase.query(TABLE_NAME, getAllColumns(), null, null, null,
                null, COLUMN_TIMESTAMP, null);

        if ( cursor != null && cursor.moveToFirst() ) {
            do {
                messageList.add(generateMessageObjectFromCursor(cursor));
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        return messageList;
    }

    @Override
    public ArrayList<Message> read(String selection, String[] selectionArgs,
                                  String groupBy, String having, String orderBy) {
        ArrayList<Message> messageList = new ArrayList<>();
        Cursor cursor = mSQLiteDatabase.query(TABLE_NAME, getAllColumns(), selection, selectionArgs, groupBy,
                having, orderBy);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                messageList.add(generateMessageObjectFromCursor(cursor));
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        return messageList;
    }

    @Override
    public boolean update(Message entity) {
        return true;
    }

    @Override
    public boolean delete(Message entity) {
        if (entity == null) {
            return false;
        }
        int status = mSQLiteDatabase.delete(TABLE_NAME, COLUMN_ID + "=?",
                new String[] {Integer.toString(entity.getId())});
        return status != 0;
    }

    private String[] getAllColumns() {
        return new String[] {COLUMN_ID, COLUMN_KEY_REMOTE_JID, COLUMN_USER_JID, COLUMN_KEY_FROM_ME,
                COLUMN_NEEDS_PUSH, COLUMN_DATA, COLUMN_FILE, COLUMN_TIMESTAMP};
    }

    private Message generateMessageObjectFromCursor(Cursor cursor) {
        Message message = new Message();
        message.setId(cursor.getInt(0));
        message.setKeyRemoteJid(cursor.getString(1));
        message.setUserJid(cursor.getString(2));
        message.setKeyFromMe(cursor.getInt(3));
        message.setNeedsPush(cursor.getInt(4));
        message.setData(cursor.getString(5));
        message.setFile(cursor.getInt(6));
        message.setTimestamp(cursor.getInt(7));
        return message;
    }

    private ContentValues generateContentValuesFromObject(Message message) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_KEY_REMOTE_JID, message.getKeyRemoteJid());
        contentValues.put(COLUMN_USER_JID,message.getUserJid());
        contentValues.put(COLUMN_KEY_FROM_ME, message.getKeyFromMe());
        contentValues.put(COLUMN_NEEDS_PUSH, message.getNeedsPush());
        contentValues.put(COLUMN_DATA, message.getData());
        contentValues.put(COLUMN_FILE,message.isFile());
        contentValues.put(COLUMN_TIMESTAMP, message.getTimestamp());
        return contentValues;
    }

    private int getLastId() {
        Cursor cursor = mSQLiteDatabase.query(DBHelper.SEQUENCE_TABLE_NAME,
                new String[] {DBHelper.SEQUENCE_COLUMN_SEQ}, DBHelper.SEQUENCE_COLUMN_NAME + "=?",
                new String[] {TABLE_NAME}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        return -1;
    }

}
