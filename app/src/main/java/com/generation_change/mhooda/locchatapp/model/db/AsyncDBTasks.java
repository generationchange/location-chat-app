package com.generation_change.mhooda.locchatapp.model.db;

import android.os.AsyncTask;

import com.generation_change.mhooda.locchatapp.model.Message;

/**
 * Created by himt18 on 15/11/15.
 */
public class AsyncDBTasks {
    public static void write(final MessageDataSource messageDataSource, final Message message) {
        new AsyncTask<Message, Void, Void>() {
            @Override
            protected Void doInBackground(Message... messages) {
                messageDataSource.insert(message);
                return null;
            }
        }.execute(message);
    }

    public static void write(final MessageDataSource messageDataSource, final Message message,
                             final Runnable block) {
        new AsyncTask<Message, Void, Void>() {
            @Override
            protected Void doInBackground(Message... messages) {
                messageDataSource.insert(message);
                return null;
            }

            @Override
            public void onPostExecute(Void result) {
                block.run();
            }
        }.execute(message);
    }

    public static void update(final MessageDataSource messageDataSource, final Message message) {
        new AsyncTask<Message, Void, Void>() {
            @Override
            protected Void doInBackground(Message... messages) {
                messageDataSource.update(message);
                return null;
            }
        }.execute(message);
    }

    public static void update(final MessageDataSource messageDataSource, final Message message,
                              final Runnable block) {
        new AsyncTask<Message, Void, Void>() {
            @Override
            protected Void doInBackground(Message... messages) {
                messageDataSource.update(message);
                return null;
            }

            @Override
            public void onPostExecute(Void result) {
                block.run();
            }
        }.execute(message);
    }

    public static void delete(final MessageDataSource messageDataSource, final Message message) {
        new AsyncTask<Message, Void, Void>() {
            @Override
            protected Void doInBackground(Message... messages) {
                messageDataSource.delete(message);
                return null;
            }
        }.execute(message);
    }

    public static void delete(final MessageDataSource messageDataSource, final Message message,
                              final Runnable block) {
        new AsyncTask<Message, Void, Void>() {
            @Override
            protected Void doInBackground(Message... messages) {
                messageDataSource.delete(message);
                return null;
            }

            @Override
            public void onPostExecute(Void result) {
                block.run();
            }
        }.execute(message);
    }

}
