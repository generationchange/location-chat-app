package com.generation_change.mhooda.locchatapp.global;

/**
 * Created by himt18 on 10/10/15.
 * Temporary server config class
 */
public class ServerCredentials {
    public static String HOST       =   "jabbim.cz";
    public static String SERVICE    =   "jabbim.cz";
    public static int PORT          =   5222;
    public static String USER       =   "";
    public static String PASS       =   "";
    public static String RESOURCE   =   "jabbim.cz";

}
