package com.generation_change.mhooda.locchatapp.controller.fragment;

/**
 * Created by mhooda on 10/6/2015.
 */
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.model.Chat;
import com.generation_change.mhooda.locchatapp.controller.adapter.ChatAdapter;
import com.generation_change.mhooda.locchatapp.model.db.ChatDataSource;
import com.generation_change.mhooda.locchatapp.model.db.DBHelper;
import com.generation_change.mhooda.locchatapp.model.db.SQLiteDataLoader;

import java.util.ArrayList;


public class ChatFragment extends Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<ArrayList<Chat> > {

    ArrayList<Chat> mChats;
    private RecyclerView mChatsView;
    private ChatAdapter mChatsAdapter;
    private AppCompatActivity mActivity;
    //private AppCompatActivity mActivity;

    public static final int LOADER_ID = 1;

    private void setupSQLiteDataLoader() {
        mActivity.getSupportLoaderManager().initLoader(LOADER_ID, null, this);
    }

    private void setupChatsView(View view) {
        mChats = new ArrayList<>();
        mChatsView = (RecyclerView) view.findViewById(R.id.chat_recycler_view);
        mChatsView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
    public void addChat(String userJid) {
        Chat newChat = new Chat();
        newChat.setKeyRemoteJid(userJid);
        mChats.add(newChat);
        mChatsAdapter.notifyDataSetChanged();
    }

    private void setupChatsAdapter() {
        mChatsAdapter = new ChatAdapter(mActivity, mChats);
        mChatsView.setAdapter(mChatsAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_list_fragment, container, false);
        setupChatsView(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (AppCompatActivity) this.getActivity();
        setupChatsAdapter();
        setupSQLiteDataLoader();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public android.support.v4.content.Loader<ArrayList<Chat> > onCreateLoader(int id, Bundle args) {
        DBHelper dbHelper = DBHelper.getInstance(mActivity);
        SQLiteDatabase sqLiteDatabase = dbHelper.getDB();
        ChatDataSource chatDataSource = new ChatDataSource(sqLiteDatabase);
        SQLiteDataLoader<Chat> sqLiteDataLoader = new SQLiteDataLoader(mActivity, chatDataSource, null, null, null,
                null, ChatDataSource.COLUMN_SORT_TIMESTAMP);
        return sqLiteDataLoader;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<ArrayList<Chat> > loader,
                               ArrayList<Chat> data) {
        mChats.clear();
        for(Chat chat:data) {
            mChats.add(chat);
        }
        mChatsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<ArrayList<Chat> > loader) {
        mChats.clear();
        mChatsAdapter.notifyDataSetChanged();
    }
}
