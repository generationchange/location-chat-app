package com.generation_change.mhooda.locchatapp.global;

/**
 * Created by varun on 10/16/2015.
 */
public class GlobalDataContainer {
    public static final String RECEIVE_MSG = "com.locationchatapp.fromserver.newmessage";
    public static final String SEND_MESSAGE = "com.locationchatapp.toserver.sendmessage";
    public static final String RECEIVE_ROSTER = "com.locationchatapp.fromserver.newroster";

    public static final String CHAT_BUNDLE = "com.loc.chatBundle";
    public static final String MESSAGE_BUNDLE = "com.loc.messageBundle";
    public static final String ROSTER_BUNDLE = "com.loc.rosterBundle";

    public static final String SENDER_CREDENTIALS = "com.loc.sender_credentials";
    public static final String RECEIVER_CREDENTIALS = "com.loc.receiver_credentials";
    public static final String RECEIVER_CREDENTIALS_JID = "com.loc.receiver_credentials.jid";
    public static final String CONNECTION_STATUS_CHANGE = "com.loc.connection_state_change";


    //Connection related
    private static boolean _s_isServiceCreated = false;
    public static enum ConnectionState {
        CONNECTED, CONNECTING, RECONNECTING, DISCONNECTED;
    }
    private static ConnectionState _s_connectionState = ConnectionState.DISCONNECTED;

    public static void setConnectionState(ConnectionState newConnectionState){
        _s_connectionState = newConnectionState;
    }
    public static ConnectionState getConnectionState(){
        return _s_connectionState;
    }

    public static void setServiceCreated(boolean isServiceCreated){
        _s_isServiceCreated = isServiceCreated;
    }
    public static boolean isServiceExists(){
        return _s_isServiceCreated;
    }

    public static void setUserJIDAndPassword(String UserName){
        String UserJid = "nextgen_" + UserName.toLowerCase();
        String PWD = "nextgen_" + UserName.toLowerCase();
        ServerCredentials.USER = UserJid;
        ServerCredentials.PASS = PWD;

    }
    public static String getReceiverJid(String UserName){
        return "nextgen_"+UserName.toLowerCase()+"@jabbim.cz";
    }

}
