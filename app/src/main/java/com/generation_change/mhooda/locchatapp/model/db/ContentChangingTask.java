package com.generation_change.mhooda.locchatapp.model.db;

import android.os.AsyncTask;
import android.support.v4.content.Loader;

/**
 * Created by himt18 on 10/11/15.
 */
public abstract class ContentChangingTask<T1, T2, T3> extends AsyncTask<T1, T2, T3> {
    private Loader<?> mLoader = null;

    public ContentChangingTask(Loader<?> loader) {
        mLoader = loader;
    }

    @Override
    public void onPostExecute(T3 result) {
        mLoader.onContentChanged();
    }
}
