package com.generation_change.mhooda.locchatapp.controller.fragment;

/**
 * Created by mhooda on 10/6/2015.
 */
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;

import com.generation_change.mhooda.locchatapp.R;

public class TrendingFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.trending, container, false);
    }
}
