package com.generation_change.mhooda.locchatapp.controller.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.widget.ShareDialog;
import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.activity.MainActivity;
import com.generation_change.mhooda.locchatapp.global.AppSession;
import com.generation_change.mhooda.locchatapp.global.JSONParser;
import com.generation_change.mhooda.locchatapp.global.User;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by anonymous on 12/7/15.
 */
public class FacebookFragment extends Fragment {

    private String info;
    private LoginButton loginButton;
    private TextView textView;
    private String uid;
//
//
//
//
//
//    private static final String PERMISSION = "publish_actions";
//    private static final Location SEATTLE_LOCATION = new Location("") {
//        {
//            setLatitude(47.6097);
//            setLongitude(-122.3331);
//        }
//    };

    private final String PENDING_ACTION_BUNDLE_KEY =
            "com..example.hellofacebook:PendingAction";

    private Button postStatusUpdateButton;
    private Button postPhotoButton;
    private ProfilePictureView profilePictureView;
    private TextView greeting;
  //  private PendingAction pendingAction = PendingAction.NONE;
    private boolean canPresentShareDialog;

    private boolean canPresentShareDialogWithPhotos;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private ShareDialog shareDialog;
    private String profilePicUrl;
    private String photoUrl;
    private AppSession session;
    private User mFacebookUser;

//    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
//        @Override
//        public void onCancel() {
//            Log.d("HelloFacebook", "Canceled");
//        }
//
//        @Override
//        public void onError(FacebookException error) {
//            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
//            String title = getString(R.string.facebook_error);
//            String alertMessage = error.getMessage();
//            showResult(title, alertMessage);
//        }
//
//        @Override
//        public void onSuccess(Sharer.Result result) {
//            Log.d("HelloFacebook", "Success!");
//            if (result.getPostId() != null) {
//                String title = getString(R.string.facebook_success);
//                String id = result.getPostId();
//                String alertMessage = getString(R.string.facebook_successfully_posted_post, id);
//                showResult(title, alertMessage);
//            }
//        }
//
//        private void showResult(String title, String alertMessage) {
//            new AlertDialog.Builder(getActivity())
//                    .setTitle(title)
//                    .setMessage(alertMessage)
//                    .setPositiveButton(R.string.facebook_ok, null)
//                    .show();
//        }
//    };
//


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity());
        mFacebookUser = new User();

        // Other app specific specialization




    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_facebook, parent, false);
//          loginButton = (LoginButton) v.findViewById(R.id.loginButton);
//        textView = (TextView)v.findViewById(R.id.textview1);

//        loginButton.setReadPermissions("user_friends");
        // If using in a fragment
//        loginButton.setFragment(this);



        session = AppSession.get(getActivity());
        callbackManager = CallbackManager.Factory.create();
//         Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                Toast toast = Toast.makeText(getActivity(), "Logged In", Toast.LENGTH_SHORT);
                toast.show();




//                handlePendingAction();
//                updateUI();
    }

            @Override
            public void onCancel() {
                // App code
//                if (pendingAction != PendingAction.NONE) {
//                    showAlert();
//                    pendingAction = PendingAction.NONE;
//                }
//                updateUI();
            }

            @Override
            public void onError(FacebookException exception) {
//                 App code
//
//                if (pendingAction != PendingAction.NONE
//                        && exception instanceof FacebookAuthorizationException) {
//                    showAlert();
//                    pendingAction = PendingAction.NONE;
//                }
//                updateUI();

            }

//            private void showAlert() {
//                new AlertDialog.Builder(getActivity())
//                        .setTitle(R.string.facebook_cancelled)
//                        .setMessage(R.string.facebook_permission_not_granted)
//                        .setPositiveButton(R.string.facebook_ok, null)
//                        .show();
//            }
//
        });
//        shareDialog = new ShareDialog(this);
//        shareDialog.registerCallback(
//                callbackManager,
//                shareCallback);
//
        if (savedInstanceState != null) {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
//            pendingAction = PendingAction.valueOf(name);
        }

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
//                updateUI();
                // It's possible that we were waiting for Profile to be populated in order to
                // post a status update.
//                handlePendingAction();
            }
        };


//        profilePictureView = (ProfilePictureView) v.findViewById(R.id.profilePicture);
//        greeting = (TextView) v.findViewById(R.id.greeting);

//        postStatusUpdateButton = (Button) v.findViewById(R.id.postStatusUpdateButton);
//        postStatusUpdateButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                onClickPostStatusUpdate();
//            }
//        });
//
//        postPhotoButton = (Button) v.findViewById(R.id.postPhotoButton);
//        postPhotoButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                onClickPostPhoto();
//            }
//        });
//
//        // Can we present the share dialog for regular links?
//        canPresentShareDialog = ShareDialog.canShow(
//                ShareLinkContent.class);
//
//        // Can we present the share dialog for photos?
//        canPresentShareDialogWithPhotos = ShareDialog.canShow(
//                SharePhotoContent.class);
//
//

        Button btn_fb_login = (Button)v.findViewById(R.id.btn_fb_login);


        btn_fb_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile","email", "user_friends"));






                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                if(object != null) {

                                    mFacebookUser = JSONParser.parseJSONFacebook(object.toString());

                                    photoUrl = "http://graph.facebook.com/"+uid+"/picture?type=large";
                                    Toast t = Toast.makeText(getActivity(),object.toString(), Toast.LENGTH_SHORT);
                                    session.createLoginSession(mFacebookUser.getName(),mFacebookUser.getEmail(),mFacebookUser.getPassword(),mFacebookUser.getPhotoURL());

                                    t.show();

                                    Intent i = new Intent(getActivity(),MainActivity.class);
                                    startActivity(i);



//                                    new GraphRequest(
//                                            AccessToken.getCurrentAccessToken(), photoUrl,null,
//                                            HttpMethod.GET,
//                                            new GraphRequest.Callback() {
//                                                public void onCompleted(GraphResponse response) {
//                            /* handle the result */
//                                                    if(response != null) {
//                                                        // Application code
//
//                                                        Toast t = Toast.makeText(getActivity(), response.toString().toString(), Toast.LENGTH_SHORT);
//                                                        t.show();
//                                                    }
//
//
//                                                }
//                                            }
//                                    ).executeAsync();
//



                                    //   JSONObject = new JSONObject(object.toString());
                                    // Application code
//                                    Log.d("Facebook",object.toString());
//                                    Toast t1 = Toast.makeText(getActivity(), object.toString(), Toast.LENGTH_SHORT);
//                                   t1.show();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields","id,name,link,email");
                request.setParameters(parameters);
                request.executeAsync();
            }
        });



//        loginButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends"));
//
//
//
//            }
//        });
          return v;
    }



    @Override
    public void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
       AppEventsLogger.activateApp(getActivity());
//
//        updateUI();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.deactivateApp(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        profileTracker.stopTracking();
    }



//    private void updateUI() {
//        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;
//
//        postStatusUpdateButton.setEnabled(enableButtons || canPresentShareDialog);
//        postPhotoButton.setEnabled(enableButtons || canPresentShareDialogWithPhotos);
//
//        Profile profile = Profile.getCurrentProfile();
//        if (enableButtons && profile != null) {
//            profilePictureView.setProfileId(profile.getId());
//            greeting.setText(getString(R.string.facebook_hello_user, profile.getFirstName()));
//        } else {
//            profilePictureView.setProfileId(null);
//            greeting.setText(null);
//        }
//    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

//        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }
//
//    private void handlePendingAction() {
//        PendingAction previouslyPendingAction = pendingAction;
//        // These actions may re-set pendingAction if they are still pending, but we assume they
//        // will succeed.
//        pendingAction = PendingAction.NONE;
//
//        switch (previouslyPendingAction) {
//            case NONE:
//                break;
//            case POST_PHOTO:
//                postPhoto();
//                break;
//            case POST_STATUS_UPDATE:
//                postStatusUpdate();
//                break;
//        }
//    }
//
//    private void onClickPostStatusUpdate() {
//        performPublish(PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);
//    }
//
//    private void postStatusUpdate() {
//        Profile profile = Profile.getCurrentProfile();
//        ShareLinkContent linkContent = new ShareLinkContent.Builder()
//                .setContentTitle("Hello Facebook")
//                .setContentDescription(
//                        "The 'Hello Facebook' sample  showcases simple Facebook integration")
//                .setContentUrl(Uri.parse("http://developers.facebook.com/docs/android"))
//                .build();
//        if (canPresentShareDialog) {
//            shareDialog.show(linkContent);
//        } else if (profile != null && hasPublishPermission()) {
//            ShareApi.share(linkContent, shareCallback);
//        } else {
//            pendingAction = PendingAction.POST_STATUS_UPDATE;
//        }
//    }
//
//    private void onClickPostPhoto() {
//        performPublish(PendingAction.POST_PHOTO, canPresentShareDialogWithPhotos);
//    }
//
//    private void postPhoto() {
//        Bitmap image = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher);
//        SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(image).build();
//        ArrayList<SharePhoto> photos = new ArrayList<>();
//        photos.add(sharePhoto);
//
//        SharePhotoContent sharePhotoContent =
//                new SharePhotoContent.Builder().setPhotos(photos).build();
//        if (canPresentShareDialogWithPhotos) {
//            shareDialog.show(sharePhotoContent);
//        } else if (hasPublishPermission()) {
//            ShareApi.share(sharePhotoContent, shareCallback);
//        } else {
//            pendingAction = PendingAction.POST_PHOTO;
//            // We need to get new permissions, then complete the action when we get called back.
//            LoginManager.getInstance().logInWithPublishPermissions(
//                    this,
//                    Arrays.asList(PERMISSION));
//        }
//    }
//
//    private boolean hasPublishPermission() {
//        AccessToken accessToken = AccessToken.getCurrentAccessToken();
//        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
//    }
//
//    private void performPublish(PendingAction action, boolean allowNoToken) {
//        AccessToken accessToken = AccessToken.getCurrentAccessToken();
//        if (accessToken != null || allowNoToken) {
//            pendingAction = action;
//            handlePendingAction();
//        }
//    }
}





