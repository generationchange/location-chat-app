package com.generation_change.mhooda.locchatapp.model.smack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.generation_change.mhooda.locchatapp.global.GlobalDataContainer;
import com.generation_change.mhooda.locchatapp.global.ServerCredentials;
import com.generation_change.mhooda.locchatapp.model.db.AsyncDBTasks;
import com.generation_change.mhooda.locchatapp.model.db.DBHelper;
import com.generation_change.mhooda.locchatapp.model.db.MessageDataSource;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ChatMessageListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;


//Smack connection manager should be connection listeners only
public class SmackConnectionManager implements ConnectionListener {

    private final String    TAG = "SCM";
    private final Context   mServiceContext;

   //Static
    static  private XMPPTCPConnection mConnection;
//    static  private GlobalDataContainer.ConnectionState mConnectionState;

    //Private
    private SmackChatManager            mChatManager;
    private SmackPingFailedListener     mPingFailedListener;
    private SmackRosterListener         mRosterListener;



    private ArrayList<String> mRoster;

    public SmackConnectionManager(Context pContext) {
        Log.i(TAG + "_INFO_01", "Constructing Smack Connection Manager.");

        //mParentThread = thread;
        mServiceContext = pContext.getApplicationContext();
        init();
    }

    public static  XMPPConnection getXMPPConnection() {
        return mConnection;
    }
    private void init()
    {
        try {
            initConnection();
        } catch (SmackException e) {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
            e.printStackTrace();
        } catch (IOException e) {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
            e.printStackTrace();
        } catch (XMPPException e) {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
            e.printStackTrace();
        }
        initPingFailListener();
        initChatManager();
        initRosterListener();

    }
    private void initPingFailListener() {
        mPingFailedListener = new SmackPingFailedListener(mConnection);
    }
    private void initChatManager(){
        mChatManager = new SmackChatManager(mServiceContext);
        ChatManager.getInstanceFor(mConnection).addChatListener(mChatManager);
    }
    private void initRosterListener(){
        mRosterListener = new SmackRosterListener(mServiceContext);
    }

    public void initConnection() throws IOException, XMPPException, SmackException {
        Log.i(TAG, "connect()");

        XMPPTCPConnectionConfiguration.XMPPTCPConnectionConfigurationBuilder builder = XMPPTCPConnectionConfiguration.builder();
        builder.setServiceName(ServerCredentials.SERVICE);
        builder.setResource(ServerCredentials.RESOURCE);
        builder.setUsernameAndPassword(ServerCredentials.USER, ServerCredentials.PASS);
        builder.setRosterLoadedAtLogin(true);
        builder.setHost(ServerCredentials.HOST);
        builder.setPort(ServerCredentials.PORT);
        builder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        //builder.setCompressionEnabled(true);
        //builder.setDebuggerEnabled(true);
        //builder.setEnabledSSLProtocols()
        //;builder.

        mConnection = new XMPPTCPConnection(builder.build());
        //Toast.makeText(mServiceContext, "connecting ......." , Toast.LENGTH_LONG).show();

        //Set ConnectionListener here to catch initial connect();
        mConnection.addConnectionListener(this);
        mConnection.setPacketReplyTimeout(15000);
        mConnection.connect();
        //SASLAuthentication.supportSASLMechanism("PLAIN", 0);
        mConnection.login();
    }

    public void end() {
        mPingFailedListener.end();
        mRosterListener.end();
        disconnect();
    }

    private void disconnect() {
        Log.i(TAG, "disconnect()");
        try {
            if(mConnection != null){
                mConnection.disconnect();
            }
        } catch (SmackException.NotConnectedException e) {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
            e.printStackTrace();
        }

        mConnection = null;
        mChatManager.unRegisterReceiver();
    }

    //ConnectionListener
    static public GlobalDataContainer.ConnectionState getConnectionState(){
        return GlobalDataContainer.getConnectionState();
    }

    public void sendConnectionChangeBroadcast(){
        Intent intent = new Intent(GlobalDataContainer.CONNECTION_STATUS_CHANGE);
        mServiceContext.sendBroadcast(intent);
    }
    @Override
    public void connected(XMPPConnection connection) {
        sendConnectionChangeBroadcast();
        GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.CONNECTED);
        Log.i(TAG, "connected()");
    }

    @Override
    public void authenticated(XMPPConnection connection) {
        sendConnectionChangeBroadcast();
        GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.CONNECTED);
        Log.i(TAG, "authenticated()");
    }

    @Override
    public void connectionClosed() {
        sendConnectionChangeBroadcast();
        GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
        Log.i(TAG, "connectionClosed()");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        sendConnectionChangeBroadcast();
        GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
        Log.i(TAG, "connectionClosedOnError()");
    }

    @Override
    public void reconnectingIn(int seconds) {
        sendConnectionChangeBroadcast();
        GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.RECONNECTING);
        Log.i(TAG, "reconnectingIn()");
    }

    @Override
    public void reconnectionSuccessful() {
        sendConnectionChangeBroadcast();
        GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.CONNECTED);
        Log.i(TAG, "reconnectionSuccessful()");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        sendConnectionChangeBroadcast();
        GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
        Log.i(TAG, "reconnectionFailed()");
    }

}

//Deriving Roster Listener.
class SmackRosterListener implements RosterListener {
    //TODO : Implement roster manual subsciption.

    private Context mServiceContext;
    //RosterListener
    SmackRosterListener (Context pContext) {
        mServiceContext = pContext;
        SmackConnectionManager.getXMPPConnection().getRoster().addRosterListener(this);
    }
    private void rebuildRoster() {
        ArrayList<String> roster = new ArrayList();
        String status;
        XMPPConnection xmppConnection = SmackConnectionManager.getXMPPConnection();
        for (RosterEntry entry : xmppConnection.getRoster().getEntries()) {
            if(xmppConnection.getRoster().getPresence(entry.getUser()).isAvailable()){
                status = "1";
            } else {
                status = "0";
            }
            roster.add(entry.getUser()+ ": " + status);
        }

        Intent intent = new Intent(GlobalDataContainer.RECEIVE_ROSTER);
        intent.setPackage(mServiceContext.getPackageName());
        intent.putStringArrayListExtra(GlobalDataContainer.ROSTER_BUNDLE, roster);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }
        mServiceContext.sendBroadcast(intent);
    }
    @Override
    public void entriesAdded(Collection<String> addresses) {
        Log.i("SRL_INFO_01", "entriesAdded()");
        rebuildRoster();
    }

    @Override
    public void entriesUpdated(Collection<String> addresses) {
        Log.i("SRL_INFO_02", "entriesUpdated()");
        rebuildRoster();
    }

    @Override
    public void entriesDeleted(Collection<String> addresses) {
        Log.i("SRL_INFO_03", "entriesDeleted()");
        rebuildRoster();
    }

    @Override
    public void presenceChanged(Presence presence) {
        Log.i("SRL_INFO_04", "presenceChanged()");
        rebuildRoster();
    }

    public void end() {

    }
}

//Deriving Chat Manager Listener. This class will interact with ChatMessageListener.
class SmackPingFailedListener implements PingFailedListener {
    //PingFailedListener

    SmackPingFailedListener(XMPPConnection pConnection) {
        super();
        PingManager.setDefaultPingInterval(600); //Ping every 10 minutes
        PingManager pingManager = PingManager.getInstanceFor(pConnection);
        pingManager.registerPingFailedListener(this);
    }
    @Override
    public void pingFailed() {
        Log.i("SPFL_WARN_01", "pingFailed()");
    }

    public void end() {}
}

//Deriving Chat Manager Listener. This class will interact with ChatMessageListener.
class SmackChatManager implements ChatManagerListener {

    private SmackChatMessageListener mChatMessageListener;
    SmackChatManager(Context pContext) {
        super();
        mChatMessageListener = new SmackChatMessageListener(pContext);
    }

    public void end() {
        mChatMessageListener.end();
    }

    @Override
    public void chatCreated(Chat chat, boolean createdLocally) {
        Log.i("SCM_INFO_01", "SmackChatManager chatCreated Routine");
        //TODO : Different message listener for every chatCreated call.
        chat.addMessageListener(mChatMessageListener);
    }

    public void unRegisterReceiver(){
        mChatMessageListener.unRegisterReceiver();
    }

}

//Deriving Chat Message Listener.
class SmackChatMessageListener implements ChatMessageListener {
//Send and Receive should work on different threads.


    private MessageDataSource mMessageDataSource;

    private  MessageSendBroadcastReceiver mReceiver;
    private  Context mServiceContext;

    //Broadcast receiver class
    class MessageSendBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != GlobalDataContainer.SEND_MESSAGE) return;
            String action = intent.getAction();
            Log.d("MSG_BROADCAST_01", "Take " + action);
            final com.generation_change.mhooda.locchatapp.model.Message message = intent.getExtras().
                    getParcelable(GlobalDataContainer.MESSAGE_BUNDLE);

            AsyncDBTasks.write(mMessageDataSource, message, new Runnable() {
                @Override
                public void run() {
                    sendMessage(message.getData(), message.getKeyRemoteJid());
                }
            });

            Toast.makeText(context, "Message: " + message.getKeyRemoteJid() + ", "
                            + message.getKeyFromMe() + ", "
                            + message.getNeedsPush() + ", "
                            + message.getData() + ", "
                            + message.getData(),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setupMessageDataSource() {
        DBHelper dbHelper = DBHelper.getInstance(mServiceContext);
        SQLiteDatabase sqLiteDatabase = dbHelper.getDB();
        mMessageDataSource = new MessageDataSource(sqLiteDatabase);
    }

    SmackChatMessageListener(Context pContext) {
        super();
        mServiceContext = pContext;
        initBroadcastReceiver();
        setupMessageDataSource();
    }

    public void end() {
        mServiceContext.unregisterReceiver(mReceiver);
    }

    private void initBroadcastReceiver()
    {
        if (mReceiver == null) {
            mReceiver = new MessageSendBroadcastReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(GlobalDataContainer.SEND_MESSAGE);
            mServiceContext.registerReceiver(mReceiver, filter);
        }
    }

    private com.generation_change.mhooda.locchatapp.model.Message getMessageFromSmackMessage(Message smackMessage) {
        com.generation_change.mhooda.locchatapp.model.Message message =
                new com.generation_change.mhooda.locchatapp.model.Message();
        message.setData(smackMessage.getBody());
        message.setKeyFromMe(0);
        message.setTimestamp((int) System.currentTimeMillis() / 1000);
        message.setKeyRemoteJid(smackMessage.getFrom().replaceAll("/.*", ""));
        message.setNeedsPush(1);
        return message;
    }

    private Intent getChatIntent(com.generation_change.mhooda.locchatapp.model.Message message) {

        Intent intent = new Intent(GlobalDataContainer.RECEIVE_MSG);
        intent.setPackage(mServiceContext.getPackageName());

        intent.putExtra(GlobalDataContainer.MESSAGE_BUNDLE, message);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }
        return intent;
    }


    @Override
    public void processMessage(Chat chat, Message smackMessage) {
        Log.i("SCML_INFO_01", "derived processMessage ChatMessageListener");
        if (smackMessage.getType().equals(Message.Type.chat) || smackMessage.getType().equals(Message.Type.normal)) {
            if (smackMessage.getBody() != null) {
                final com.generation_change.mhooda.locchatapp.model.Message message =
                        getMessageFromSmackMessage(smackMessage);
                AsyncDBTasks.write(mMessageDataSource, message, new Runnable() {
                    @Override
                    public void run() {
                        mServiceContext.sendBroadcast(getChatIntent(message));
                        Log.i("SCML_INFO_01", "processMessage message broadcast to activity.");
                    }
                });
            }
            /*********************************************
             * We can also get properties on messages
             * *******************************************

             *********************************************/
        }
    }

    public void unRegisterReceiver() {
        if(mReceiver != null) {
            mServiceContext.unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }
    //send message and connect reconnect should be on a different thread.
    public void sendMessage(String body, String toJid) {
        Log.i("SCML_INFO_02", "send message to server");

        Chat chat = ChatManager.getInstanceFor(SmackConnectionManager.getXMPPConnection()).createChat(toJid, this);
        try {

            if (SmackConnectionManager.getConnectionState() == GlobalDataContainer.ConnectionState.CONNECTED) {
                chat.sendMessage(body);
                /*********************************************
                 * We can also set properties on messages as follows
                 * *********************************************
                Message newMessage = new Message();
                newMessage.setBody(body);
                newMessage.setProperty("favoriteColor", "red");
                chat.sendMessage(newMessage);
                *********************************************/
            }
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }

    }

}



