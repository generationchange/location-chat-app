package com.generation_change.mhooda.locchatapp.controller.activity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.fragment.CreateNewGroup;

/**
 * Created by manjeet on 9/12/15.
 */
public class NewGroupActivity extends AppCompatActivity {

    public String mGpToolbarName = "New Group";
    public Toolbar mtoolbar;

    FragmentManager fm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interest_activity);

        setupToolbar();
        initiate_fragment();
    }

    private void initiate_fragment(){
        fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.interest_fragment_container);
        if (fragment == null) {
            fragment = new CreateNewGroup();
            fm.beginTransaction()
                    .add(R.id.interest_fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getGpName(){
        return mtoolbar.getTitle().toString();
    }

    private void setupToolbar(){
        mtoolbar = (Toolbar) findViewById(R.id.interest_toolbar);
        mtoolbar.setTitle(mGpToolbarName);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

}