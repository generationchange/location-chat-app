package com.generation_change.mhooda.locchatapp.controller.fragment;

import android.support.v4.app.Fragment;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.activity.ChatContactInfo;
import com.generation_change.mhooda.locchatapp.controller.activity.InterestActivity;
import com.generation_change.mhooda.locchatapp.controller.adapter.ChatAdapter;
import com.generation_change.mhooda.locchatapp.controller.adapter.InterestAdapter;
import com.generation_change.mhooda.locchatapp.model.Chat;
import com.generation_change.mhooda.locchatapp.model.Group;
import com.generation_change.mhooda.locchatapp.model.db.ChatDataSource;
import com.generation_change.mhooda.locchatapp.model.db.DBHelper;
import com.generation_change.mhooda.locchatapp.model.db.SQLiteDataLoader;

import java.util.ArrayList;

/**
 * Created by manjeet on 9/12/15.
 */
public class InterestFragment extends Fragment {

    private RecyclerView mInterestView;
    private InterestAdapter mInterestAdapter;
    private AppCompatActivity mActivity;
    private InterestActivity interestActivity;
    ArrayList<Group> mGroups;
    //private AppCompatActivity mActivity;

    private void setupInterestView(View view) {
        mInterestView = (RecyclerView) view.findViewById(R.id.interest_fragment_rv);
        mInterestView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void setupInterestAdapter() {
        mInterestAdapter = new InterestAdapter(mActivity, mGroups);
        mInterestView.setAdapter(mInterestAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mGroups = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.interest_fragment, container, false);

        interestActivity = (InterestActivity) getActivity();
        mGroups = interestActivity.getSharedList();

        setupInterestView(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (AppCompatActivity) this.getActivity();

        setupInterestAdapter();


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


