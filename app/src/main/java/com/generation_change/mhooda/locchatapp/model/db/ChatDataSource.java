package com.generation_change.mhooda.locchatapp.model.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.generation_change.mhooda.locchatapp.model.Chat;

import java.util.ArrayList;

/**
 * Created by himt18 on 08/11/15.
 */
public class ChatDataSource extends DataSource<Chat> {

    public static final String TABLE_NAME = "chat_list";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_KEY_REMOTE_JID = "key_remote_jid";
    public static final String COLUMN_MESSAGE_TABLE_ID = "message_table_id";
    public static final String COLUMN_SORT_TIMESTAMP = "sort_timestamp";

    public ChatDataSource(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
    }

    @Override
    public boolean insert(Chat entity) {
        if (entity == null) {
            return false;
        }
        int status = (int) mSQLiteDatabase.insert(TABLE_NAME, null,
                generateContentValuesFromObject(entity));
        return status != -1;
    }

    @Override
    public ArrayList<Chat> read() {
        ArrayList<Chat> chatList = new ArrayList<>();
        // fetches data from db
        Cursor cursor = mSQLiteDatabase.query(TABLE_NAME, null, null, null, null,null,
                COLUMN_SORT_TIMESTAMP, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                chatList.add(generateChatObjectFromCursor(cursor));
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        return chatList;
    }

    @Override
    public ArrayList<Chat> read(String selection, String[] selectionArgs,
                                String groupBy, String having, String orderBy) {
        ArrayList<Chat> chatList = new ArrayList<>();
        Cursor cursor = mSQLiteDatabase.query(TABLE_NAME, null, selection, selectionArgs, groupBy,
                having, orderBy);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                chatList.add(generateChatObjectFromCursor(cursor));
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        return chatList;
    }

    @Override
    public boolean update(Chat entity) {
        if (entity == null)
            return false;
        int status = mSQLiteDatabase.update(TABLE_NAME, generateContentValuesFromObject(entity),
                COLUMN_KEY_REMOTE_JID + " =? ", new String[] {entity.getKeyRemoteJid()} );
        return status != 0;
    }

    @Override
    public boolean delete(Chat entity) {
        int status = mSQLiteDatabase.delete(TABLE_NAME, COLUMN_ID + "=?",
                new String[] {Integer.toString(entity.getId())});
        return status != 0;
    }

    private String[] getAllColumns() {
        return new String[] {COLUMN_ID, COLUMN_KEY_REMOTE_JID, COLUMN_MESSAGE_TABLE_ID,
                COLUMN_SORT_TIMESTAMP};
    }

    private ContentValues generateContentValuesFromObject(Chat chat) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_KEY_REMOTE_JID, chat.getKeyRemoteJid());
        contentValues.put(COLUMN_MESSAGE_TABLE_ID, chat.getMessageTableId());
        contentValues.put(COLUMN_SORT_TIMESTAMP, chat.getmSortTimestamp());
        return contentValues;
    }

    private Chat generateChatObjectFromCursor(Cursor cursor) {
        Chat chat = new Chat();
        chat.setId(cursor.getInt(0));
        chat.setKeyRemoteJid(cursor.getString(1));
        chat.setMessageTableId(cursor.getInt(2));
        chat.setmSortTimestamp(cursor.getInt(3));
        return chat;

    }

    private int getLastId() {
        Cursor cursor = mSQLiteDatabase.query(DBHelper.SEQUENCE_TABLE_NAME,
                new String[] {DBHelper.SEQUENCE_COLUMN_SEQ}, DBHelper.SEQUENCE_COLUMN_NAME + "=?",
                new String[] {TABLE_NAME}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        return -1;
    }

}
