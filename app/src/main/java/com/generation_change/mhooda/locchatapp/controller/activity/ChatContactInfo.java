package com.generation_change.mhooda.locchatapp.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.adapter.ChatAdapter;
import com.generation_change.mhooda.locchatapp.controller.adapter.InterestAdapter;
import com.generation_change.mhooda.locchatapp.model.Chat;
import com.generation_change.mhooda.locchatapp.model.Group;
import com.generation_change.mhooda.locchatapp.model.db.ChatDataSource;
import com.generation_change.mhooda.locchatapp.model.db.DBHelper;
import com.generation_change.mhooda.locchatapp.model.db.SQLiteDataLoader;

import java.util.ArrayList;

/**
 * Created by mhooda on 10/4/2015.
 */
public class ChatContactInfo extends AppCompatActivity {

    private String NAME;
    private String ContactPhoto;
    private String LASTSEEN;
    private String STATUS = "Lets finish till new year :)";
    private String PHONE_NUM = "9654780715";
    private String MAIL_ID = "lets_try@gmail.com";
    private Context mContext;
    private boolean RV_ENABLER = false;

    //shared interest
    ArrayList<Group> mGroups;
    private RecyclerView mSharedInterestView;
    private InterestAdapter mInterestAdapter;
    public static final int LOADER_ID = 1;

    //other interest
    ArrayList<Group> mOtherGroups;
    private RecyclerView mOtherInterestView;
    private InterestAdapter mOtherInterestAdapter;

    //Menu Item
    private String rv_enabler = "Enable Interest Scroll";

    LinearLayout sview;
    LinearLayout oview ;


    private void setupInterestView(View view) {
        //shared Interest
        mGroups = new ArrayList<>();
        mSharedInterestView = (RecyclerView) view.findViewById(R.id.profile_interest_rv);
        mSharedInterestView.setLayoutManager(new LinearLayoutManager(this));
        mSharedInterestView.setNestedScrollingEnabled(RV_ENABLER);

    }

    private void setupOtherInterestView(View view) {
        //Other Interest
        mOtherGroups = new ArrayList<>();
        mOtherInterestView = (RecyclerView) view.findViewById(R.id.profile_other_rv);
        mOtherInterestView.setLayoutManager(new LinearLayoutManager(this));
        mOtherInterestView.setNestedScrollingEnabled(RV_ENABLER);
    }

    public void addGroup(String name, String Num, String description,int photo_id) {
        Group newGroup = new Group();
        newGroup.setmGpName(name);
        newGroup.setmGpNumMem(Num);
        newGroup.setmPhotoId(photo_id);
        newGroup.setmDescription(description);
        mGroups.add(newGroup);
        //mInterestAdapter.notifyDataSetChanged();
    }

    public void addOtherGroup(String name, String Num, String description ,int photo_id) {
        Group newGroup = new Group();
        newGroup.setmGpName(name);
        newGroup.setmGpNumMem(Num);
        newGroup.setmPhotoId(photo_id);
        newGroup.setmDescription(description);
        mOtherGroups.add(newGroup);
        //mInterestAdapter.notifyDataSetChanged();
    }


    private void setupInterestAdapter() {
        mInterestAdapter = new InterestAdapter(this, mGroups);
        mSharedInterestView.setAdapter(mInterestAdapter);

        //Other Interest
        mOtherInterestAdapter = new InterestAdapter(this, mOtherGroups);
        mOtherInterestView.setAdapter(mOtherInterestAdapter);
    }

    private void customInterestList(){
        addGroup("Football","35 Members","Football refers to a number of sports that involve, to varying degrees, kicking a ball with the foot to score a goal",R.drawable.group);
        addGroup("Swimming", "45 Members","Football refers core a goal" ,R.drawable.group);
        addGroup("Sex", "10000 Members","", R.drawable.group);
        addGroup("Technology", "100 Members","", R.drawable.group);
        addGroup("Obama","1043 Members","", R.drawable.group);
        addGroup("USA","999 Members","",R.drawable.group);
        addGroup("Bakchodi","310000 Members","",R.drawable.group);
        addGroup("Ho gayi Backchodi", "10000 Members", "", R.drawable.group);

        addOtherGroup("Football", "35 Members", "Football refers to a number of sports that involve, to varying degrees, kicking a ball with the foot to score a goal", R.drawable.group);
        addOtherGroup("Swimming", "45 Members", "", R.drawable.group);
        addOtherGroup("Sex", "10000 Members", "", R.drawable.group);
        addOtherGroup("Technology", "100 Members", "", R.drawable.group);
        //addOtherGroup("Obama","1043 Members", R.drawable.group);
        //addOtherGroup("USA","999 Members",R.drawable.group);
        //addOtherGroup("Bakchodi","310000 Members",R.drawable.group);
        //addOtherGroup("Ho gayi Backchodi","10000 Members",R.drawable.group);


    }

    private void setupToolbar(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //toolbar.setBackgroundColor(Color.TRANSPARENT);
        TextView name = (TextView)findViewById(R.id.profile_contact_name);
        name.setText(NAME);
        TextView lastseen = (TextView)findViewById(R.id.profile_last_seen);
        lastseen.setText(LASTSEEN);

    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    public void setupImage() {
        Resources res = getResources();
        Bitmap src = BitmapFactory.decodeResource(res, getImageId(this, ContactPhoto));
        BitmapDrawable ob = new BitmapDrawable(getResources(), src);
        ImageView imageView = (ImageView)findViewById(R.id.profile_chat_pic);
        imageView.setBackground(ob);
    }

    private void setupStatus(){
        TextView status = (TextView)findViewById(R.id.profile_status);
        status.setText(STATUS);
    }

    private void setupContactDetails(){
        TextView phone = (TextView)findViewById(R.id.phone_num);
        phone.setText(PHONE_NUM);

        TextView mail = (TextView)findViewById(R.id.mail_id);
        mail.setText(MAIL_ID);
    }

    private void setupInterest(){
        RecyclerView srv = (RecyclerView)findViewById(R.id.profile_interest_rv);
        LinearLayoutManager sllm = new LinearLayoutManager(this);
        srv.setLayoutManager(sllm);

        RecyclerView orv = (RecyclerView)findViewById(R.id.profile_other_rv);
        LinearLayoutManager ollm = new LinearLayoutManager(this);
        orv.setLayoutManager(ollm);
    }

    private void setupCard(View sview, View oview){

        CardView scard = (CardView)findViewById(R.id.profile_shared_interest_card);
        CardView ocard = (CardView)findViewById(R.id.profile_other_groups_card);

        if(mGroups.size() == 0)
            scard.setVisibility(View.INVISIBLE);
        else {
            FrameLayout.LayoutParams slp = (FrameLayout.LayoutParams) sview.getLayoutParams();
            if (mGroups.size() <7) {
                slp.height = ((int) getResources().getDimension(R.dimen.profile_card_height))*mGroups.size() + (int)getResources().getDimension(R.dimen.profile_card_height_extra);

            }
            else{
                slp.height = ((int) getResources().getDimension(R.dimen.profile_card_height)*6) + (int)getResources().getDimension(R.dimen.profile_card_height_extra);

            }

        }

        if(mOtherGroups.size() == 0)
            ocard.setVisibility(View.INVISIBLE);
        else {
            FrameLayout.LayoutParams olp = (FrameLayout.LayoutParams) oview.getLayoutParams();
            if (mOtherGroups.size() < 7)
            {
                olp.height = ((int) getResources().getDimension(R.dimen.profile_card_height)*mOtherGroups.size()) + (int)getResources().getDimension(R.dimen.profile_card_height_extra);

            }
            else{
                olp.height = ((int) getResources().getDimension(R.dimen.profile_card_height))*6 + (int)getResources().getDimension(R.dimen.profile_card_height_extra);

            }

        }

    }

    //shared interest listener
    public void setInterestListener() {
        RelativeLayout slayout = (RelativeLayout) findViewById(R.id.profile_shared_interest_bar);
        slayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InterestActivity.class);
                Bundle list = new Bundle();
                list.putParcelableArrayList("LIST", mGroups);
                list.putString("Group", "Shared Interests");
                intent.putExtras(list);
                mContext.startActivity(intent);
            }
        });

        RelativeLayout olayout = (RelativeLayout) findViewById(R.id.profile_other_interest_bar);
        olayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InterestActivity.class);
                Bundle list = new Bundle();
                list.putParcelableArrayList("LIST", mOtherGroups);
                list.putString("Group", "Other Interests");
                intent.putExtras(list);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chatcontactinfo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        //else
        //    return false;
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.interest_rv_enabler:
                if(!RV_ENABLER){
                    RV_ENABLER = true;
                    rv_enabler = "Disable Interest Scroll";
                    //item.setTitle("Disable Interest Scroll");

                    setupInterest();
                    setupInterestView(sview);
                    setupOtherInterestView(oview);
                    setupInterestAdapter();
                    customInterestList();
                    setupCard(sview, oview);
                    invalidateOptionsMenu();

                }
                else{
                    RV_ENABLER = false;
                    rv_enabler = "Enable Interest Scroll";
                    //item.setTitle("Enable Interest Scroll");
                    setupInterest();
                    setupInterestView(sview);
                    setupOtherInterestView(oview);
                    setupInterestAdapter();
                    customInterestList();
                    setupCard(sview, oview);
                    invalidateOptionsMenu();

                    //rv_menu.setTitle("Enable Interest Scroll");
                }
                return true;
        }
        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.interest_rv_enabler).setTitle(rv_enabler);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_contact_info);
        mContext = this;

        final Intent intent = getIntent();
        if(intent.hasExtra("NAME"))
            NAME = intent.getExtras().getString("NAME");
        if(intent.hasExtra("PHOTO"))
            ContactPhoto = intent.getExtras().getString("PHOTO");
        if(intent.hasExtra("LASTSEEN"))
            LASTSEEN = intent.getExtras().getString("LASTSEEN");

        sview = (LinearLayout)findViewById(R.id.profile_shared_interest);
        oview = (LinearLayout)findViewById(R.id.profile_other_interest);


        setupToolbar();
        setupImage();
        setupStatus();
        setupContactDetails();
        setupInterest();
        setupInterestView(sview);
        setupOtherInterestView(oview);
        setupInterestAdapter();
        customInterestList();
        setupCard(sview, oview);
        setInterestListener();

    }


}