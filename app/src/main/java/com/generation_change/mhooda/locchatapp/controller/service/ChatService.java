
package com.generation_change.mhooda.locchatapp.controller.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;


import com.generation_change.mhooda.locchatapp.global.GlobalDataContainer;
import com.generation_change.mhooda.locchatapp.global.ServerCredentials;
import com.generation_change.mhooda.locchatapp.model.smack.SmackConnectionManager;

/**
 * Created by vasharma on 10/16/2015.
 */
public class ChatService extends Service {
    private boolean                 _m_ActiveConnection = false;
    private Thread                  _m_Thread = null;
    private ChatLocalHandler        _m_THandler;

    private ChatService             _m_ChatService ;
    private SmackConnectionManager  _m_SmackConnection = null;
    private ChatServiceBroadcastReceiver mReceiver = null;


   // private GlobalDataContainer.ConnectionState mConnectionState;

    @Override
    public void onCreate() {
        GlobalDataContainer.setServiceCreated(true);
        super.onCreate();
        _m_ChatService = this;
        initNetworkConnectivity();
        initBroadcastReceiver();
        if (GlobalDataContainer.getConnectionState() == GlobalDataContainer.ConnectionState.CONNECTED &&
                ServerCredentials.USER != "") {
            initSmackConnection();
        }
    }
    private void initBroadcastReceiver() {
        if (mReceiver == null) {
            mReceiver = new ChatServiceBroadcastReceiver();
            mReceiver.setContext(this);
            attachBroadCastReceiver();
        }
    }
    private void attachBroadCastReceiver() {
        if (mReceiver != null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(GlobalDataContainer.SENDER_CREDENTIALS);
            this.registerReceiver(mReceiver, filter);
        }
    }
    private void detachBroadCastReceiver() {
        if (mReceiver != null) {
            this.unregisterReceiver(mReceiver);
        }
    }
    @Override
    public int onStartCommand(android.content.Intent intent, int flags, int startId) {
       return  super.onStartCommand(intent, flags, startId);
    }

    private void haltConnection() {
        GlobalDataContainer.setServiceCreated(false);
        if (!(_m_Thread == null)) {
            stopThread();
        }
    }
    @Override
    public void onDestroy() {
        haltConnection();
        detachBroadCastReceiver();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void ReInitSmackConnection() {
        haltConnection();
        initSmackConnection();
    }
    public void initSmackConnection() {
        if (!_m_ActiveConnection) {
            _m_ActiveConnection = true;
            // Create ConnectionThread Loop
            if (_m_Thread == null || !_m_Thread.isAlive()) {
                _m_Thread = new Thread(new Runnable() {

                    //private ChatSenderBroadCastReceiver mReceiver ;
                    public void run() {
                        Looper.prepare();
                        _m_THandler = new ChatLocalHandler();
                        initConnection();
                        Looper.loop();
                    }
                    private void initConnection() {
                        _m_SmackConnection = new SmackConnectionManager(_m_ChatService);
                    }

                });
                _m_Thread.start();
            }
        }
    }
    private void stopThread() {
        _m_THandler.post(new Runnable() {
            @Override
            public void run() {
                if (_m_SmackConnection != null) {
                    _m_SmackConnection.end();
                    _m_SmackConnection = null;
                }
            }
        });
        _m_ActiveConnection = false;
        _m_Thread.destroy();
        _m_Thread = null;
    }

    public void initNetworkConnectivity() {

        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in air plan mode it will be null
        if (netInfo != null && netInfo.isConnected()) {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.CONNECTED);
        } else {
            GlobalDataContainer.setConnectionState(GlobalDataContainer.ConnectionState.DISCONNECTED);
        }

    }
}



class ChatLocalHandler extends Handler {

    public ChatLocalHandler(){
        super();
        Log.d("MSG_HANDLER_01", "Creating Handler");
    }

    @Override
    public void handleMessage(Message msg) {

    }
}

class ChatServiceBroadcastReceiver extends BroadcastReceiver {

    private ChatService mChatService;
    public  void setContext(ChatService pChatService) {
        mChatService = pChatService;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        //String action = intent.getAction();
        mChatService.ReInitSmackConnection();
    }

}

