package com.generation_change.mhooda.locchatapp.model;

/**
 * Created by himt18 on 08/11/15.
 */
public class Props {
    private int mId;
    private String mKey;
    private String mValue;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        this.mKey = key;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        this.mValue = value;
    }
}
