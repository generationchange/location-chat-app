package com.generation_change.mhooda.locchatapp.controller.fragment;

/**
 * Created by mhooda on 10/6/2015.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.generation_change.mhooda.locchatapp.R;

public class GroupFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.group, container, false);
    }
}