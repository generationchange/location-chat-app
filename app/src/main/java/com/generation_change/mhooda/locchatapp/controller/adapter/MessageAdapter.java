package com.generation_change.mhooda.locchatapp.controller.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.opengl.Matrix;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.GuiThreads.BitmapWorkerTask;
import com.generation_change.mhooda.locchatapp.model.Message;


import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by mhooda on 10/11/2015.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
    public static Context mContext;
    ArrayList<Message> mMessages;
    public static boolean group;
    public static boolean image;
    public static int screen_width;
    //static float scale;

    public void getDataFromActivity(boolean group, boolean image, int width){
        this.group = group;
        this.image = image;
        this.screen_width = width;
    }

    public MessageAdapter(Context context, ArrayList<Message> messages) {
        mContext = context;
        mMessages = messages;
        //scale = context.getResources().getDisplayMetrics().density;

        }
    @Override
    public MessageAdapter.MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.msg_list_view, parent,false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageAdapter.MessageHolder holder, int position) {
        Message message = mMessages.get(position);
        holder.bindMessage(message);
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    public static class MessageHolder extends RecyclerView.ViewHolder {
        private TextView send_text;
        private TextView send_text_time;
        private TextView recv_text;
        private TextView recv_text_time;
        private TextView recv_user_jid;
        private View send_box;
        private View recv_box;
        private ImageView send_img;
        private ImageView recv_img;
        private boolean viewRemoved = false;
        public int isFile;
        public MessageHolder(View itemView) {
            super(itemView);
            send_text = (TextView)itemView.findViewById(R.id.chat_message_send);
            send_text_time = (TextView)itemView.findViewById(R.id.chat_message_send_time);
            recv_text = (TextView)itemView.findViewById(R.id.chat_message_receive);
            recv_text_time = (TextView)itemView.findViewById(R.id.chat_message_receive_time);
            send_box = itemView.findViewById(R.id.layout_chat_send_box);
            recv_box = itemView.findViewById(R.id.layout_chat_receive_box);
            recv_user_jid = (TextView)itemView.findViewById(R.id.recv_user_jid);
            send_img = (ImageView)itemView.findViewById(R.id.chat_message_send_image);
            recv_img = (ImageView)itemView.findViewById(R.id.chat_message_recv_image);
        }
        public void bindMessage(Message message) {

            int keyFromMe = message.getKeyFromMe();
            isFile = message.isFile();
            if (keyFromMe == 0) {
                if(isFile == 0){
                String recv_msg = message.getData();
                //int size = recv_msg.length();
                String text = recv_msg.trim();
                if (text.length() > 0) {

                    recv_text.setText(message.getData());

                    DateFormat format = new SimpleDateFormat("h:mm a");
                    String str = format.format(message.getTimestamp());
                    recv_text_time.setText(str.toUpperCase());
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)
                            recv_text_time.getLayoutParams();
                    //int pixels = (int) (size * 20 + 0.5f);
                    recv_text.measure(0, 0);       //must call measure!
                    int width = ((int) recv_text.getMeasuredWidth()) + 15;
                    if (width < 500) {
                        layoutParams.setMargins(width, 1, 1, 1);
                        recv_text_time.setLayoutParams(layoutParams);
                    }
                    recv_text.setVisibility(View.VISIBLE);
                    recv_text_time.setVisibility(View.VISIBLE);
                    recv_img.setVisibility(View.GONE);
                    recv_box.setVisibility(View.VISIBLE);
                    if (group) {
                        recv_user_jid.setText(message.getUserJid());
                        recv_user_jid.setVisibility(View.VISIBLE);
                    } else {
                        /*if (!viewRemoved)
                        { ((ViewGroup) recv_user_jid.getParent()).removeView(recv_user_jid);
                            viewRemoved = true;
                        }*/
                        recv_user_jid.setVisibility(View.GONE);
                    }
                }
                }
                else if(isFile == 1)
                {
                    DateFormat format = new SimpleDateFormat( "h:mm a");
                    String str = format.format(message.getTimestamp());
                    recv_text_time.setText(str.toUpperCase());
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)
                            recv_text_time.getLayoutParams();
                    //int densityDpi = (int)(scale * 160f);
                    layoutParams.setMargins((int) mContext.getResources().getDimension(R.dimen.chat_timestamp), 1, 1, 1);
                    recv_text_time.setLayoutParams(layoutParams);
                    recv_text_time.setTextColor(Color.WHITE);
                    recv_text.setVisibility(View.GONE);
                    recv_text_time.setVisibility(View.VISIBLE);
                    recv_box.setVisibility(View.VISIBLE);
                    if (group) {
                        recv_user_jid.setText(message.getUserJid());
                        recv_user_jid.setVisibility(View.VISIBLE);
                    } else {
                        /*if (!viewRemoved)
                        { ((ViewGroup) recv_user_jid.getParent()).removeView(recv_user_jid);
                            viewRemoved = true;
                        }*/
                        recv_user_jid.setVisibility(View.GONE);
                    }
                    recv_img.setVisibility(View.VISIBLE);
                    setPic(recv_img, message.getData());
                }
                else {
                    recv_box.setVisibility(View.GONE);
                }
                send_box.setVisibility(View.GONE);
            } else {
                if(isFile == 0) {
                    String send_msg = message.getData();
                    //int size = send_msg.length();
                    String text = send_msg.trim();
                    if (text.length() > 0) {
                        send_text.setText(send_msg);
                        DateFormat format = new SimpleDateFormat("h:mm a");
                        String str = format.format(message.getTimestamp());
                        send_text_time.setText(str.toUpperCase());
                        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)
                                send_text_time.getLayoutParams();
                        //int densityDpi = (int)(scale * 160f);
                        send_text.measure(0, 0);       //must call measure!
                        int width = ((int) send_text.getMeasuredWidth());
                        if (width < 500) {
                            layoutParams.setMargins(width + 15, 1, 1, 1);
                            send_text_time.setLayoutParams(layoutParams);
                        }
                        send_text_time.setTextColor(mContext.getResources().getColor(R.color.timestamp_color));
                        send_text.setVisibility(View.VISIBLE);
                        send_text_time.setVisibility(View.VISIBLE);
                        send_box.setVisibility(View.VISIBLE);
                        send_img.setVisibility(View.GONE);
                    }
                }
                else if (isFile == 1){
                    DateFormat format = new SimpleDateFormat( "h:mm a");
                    String str = format.format(message.getTimestamp());
                    send_text_time.setText(str.toUpperCase());
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)
                            send_text_time.getLayoutParams();
                    //int densityDpi = (int)(scale * 160f);
                    layoutParams.setMargins((int) mContext.getResources().getDimension(R.dimen.chat_timestamp), 1, 1, 1);
                    send_text_time.setLayoutParams(layoutParams);
                    send_text_time.setTextColor(Color.WHITE);
                    send_text.setVisibility(View.GONE);
                    send_text_time.setVisibility(View.VISIBLE);
                    send_box.setVisibility(View.VISIBLE);

                    send_img.setVisibility(View.VISIBLE);
                    setPic(send_img, message.getData());
                }
                else {
                   send_box.setVisibility(View.GONE);
                }
                recv_box.setVisibility(View.GONE);
            }
        }


        public void setPic(ImageView mView, String mFilePath) {
            // Get the dimensions of the View
            float targetW = mContext.getResources().getDimension(R.dimen.chat_image_width);
            float targetH;
            float targetHL = mContext.getResources().getDimension(R.dimen.chat_image_height_landscape);
            float targetHP = mContext.getResources().getDimension(R.dimen.chat_image_height_portrait);

            Bitmap bitmap = BitmapFactory.decodeFile(mFilePath);
            if(bitmap.getHeight() > bitmap.getWidth())
            {
            //float inv_aspect_ratio = (bitmap.getHeight())/(bitmap.getWidth());
            //targetH =  inv_aspect_ratio * targetW;
                targetH = targetHP;
            }
            else
            {
                targetH = targetHL;
                //int pad_sides = (int) mContext.getResources().getDimension(R.dimen.chat_image_pad_sides);
                //mView.setPadding(pad_sides,5,pad_sides,5);
            }

            bitmap = getRoundedCornerBitmap(bitmap);

            //set imageview params
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int)targetW, (int)targetH);
            mView.setLayoutParams(layoutParams);

            mView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, (int) targetW, (int) targetH, true));



        }

        public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = mContext.getResources().getDimension(R.dimen.chat_image_round);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }

    }
}