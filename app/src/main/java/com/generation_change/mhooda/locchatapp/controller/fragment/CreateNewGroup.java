package com.generation_change.mhooda.locchatapp.controller.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.activity.NewGroupActivity;

/**
 * Created by manjeet on 9/12/15.
 */
public class CreateNewGroup extends Fragment {

    private AppCompatActivity mActivity;
    private NewGroupActivity newGroupActivity;
    private EditText mGpNameEdit;
    private TextView next;
    private String mGpName;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    private void setupGpName(){
            mGpNameEdit = (EditText)view.findViewById(R.id.create_new_gp_name);
            mGpNameEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(
                        CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(
                        CharSequence s, int start, int before, int count) {
                    mGpName = s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
    }

    private void displaywarning(){
        DialogWarning dialog = new DialogWarning();
        dialog.Title = "No Group Subject";
        dialog.Message = "Please provide group subject";
        dialog.show(getActivity().getSupportFragmentManager(), null);
    }

    private void setupNext(){
            next = (TextView)view.findViewById(R.id.create_new_gp_next);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mGpName == null || mGpName.trim().length() == 0){
                        displaywarning();
                    }
                    else{
                    ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(mGpName);
                    Fragment finalCreateGroup = new FinalCreateNewGroup();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.interest_fragment_container, finalCreateGroup, null)
                            .addToBackStack(null)
                            .commit();
                }}
            });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.create_new_gp, container, false);

        newGroupActivity = (NewGroupActivity) getActivity();
        setupGpName();
        setupNext();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (AppCompatActivity) this.getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}


