package com.generation_change.mhooda.locchatapp.model.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.generation_change.mhooda.locchatapp.model.Props;

import java.util.ArrayList;

/**
 * Created by himt18 on 08/11/15.
 */
public class PropsDataSource extends DataSource<Props> {

    public static final String TABLE_NAME = "props";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_KEY = "key";
    public static final String COLUMN_VALUE = "value";



    public PropsDataSource(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
    }

    @Override
    public boolean insert(Props entity) {
        if (entity == null) {
            return false;
        }
        int status = (int) mSQLiteDatabase.insert(TABLE_NAME, null,
                generateContentValuesFromObject(entity));
        return status != -1;
    }

    @Override
    public ArrayList<Props> read() {
        ArrayList<Props> propsList = new ArrayList<>();
        Cursor cursor = mSQLiteDatabase.query(TABLE_NAME, null, null, null,null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                propsList.add(generatePropsObjectFromCursor(cursor));
                cursor.moveToNext();
            } while (cursor.isAfterLast());
        }
        return propsList;
    }

    @Override
    public ArrayList read(String selection, String[] selectionArgs, String groupBy, String having,
                          String orderBy) {
        ArrayList<Props> propsList = new ArrayList<>();
        Cursor cursor = mSQLiteDatabase.query(TABLE_NAME, null, selection, selectionArgs, groupBy,
                having, orderBy);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                propsList.add(generatePropsObjectFromCursor(cursor));
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        return propsList;
    }

    @Override
    public boolean update(Props entity) {
        if (entity == null) {
            return false;
        }
        int status = mSQLiteDatabase.update(TABLE_NAME, generateContentValuesFromObject(entity),
                COLUMN_KEY + "=?", new String[]{entity.getValue()});
        return status != 0;
    }

    @Override
    public boolean delete(Props entity) {
        if (entity == null) {
            return false;
        }
        int status = mSQLiteDatabase.delete(TABLE_NAME, COLUMN_ID + "=?",
                new String[] {Integer.toString(entity.getId())});
        return status != 0;
    }

    private String[] getAllColumns() {
        return new String[] {COLUMN_ID, COLUMN_VALUE, COLUMN_KEY};
    }

    private Props generatePropsObjectFromCursor(Cursor cursor) {
        Props props = new Props();
        props.setId(cursor.getInt(0));
        props.setKey(cursor.getString(1));
        props.setValue(cursor.getString(2));
        return props;
    }

    private ContentValues generateContentValuesFromObject(Props props) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_KEY, props.getKey());
        contentValues.put(COLUMN_VALUE, props.getValue());
        return contentValues;
    }

    private int getLastId() {
        Cursor cursor = mSQLiteDatabase.query(DBHelper.SEQUENCE_TABLE_NAME,
                new String[] {DBHelper.SEQUENCE_COLUMN_SEQ}, DBHelper.SEQUENCE_COLUMN_NAME + "=?",
                new String[] {TABLE_NAME}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        return -1;
    }


}
