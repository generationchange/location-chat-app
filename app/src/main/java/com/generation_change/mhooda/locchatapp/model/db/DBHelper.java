package com.generation_change.mhooda.locchatapp.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by himt18 on 17/10/15.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper mDBHelper;
    private static SQLiteDatabase mSQLiteDatabase;
    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String SEQUENCE_TABLE_NAME = "sqlite_sequence";
    public static final String SEQUENCE_COLUMN_SEQ = "seq";
    public static final String SEQUENCE_COLUMN_NAME = "name";

    public static DBHelper getInstance(Context context) {
        if ( mDBHelper == null ) {
            mDBHelper = new DBHelper(context);
        }
        return mDBHelper;
    }

    public SQLiteDatabase getDB() {
        if (mSQLiteDatabase == null ) {
            mSQLiteDatabase = mDBHelper.getWritableDatabase();
        }
        return mSQLiteDatabase;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("DB", "CREATED");
        createPropsTable(sqLiteDatabase);
        createChatListTable(sqLiteDatabase);
        createMessagesTable(sqLiteDatabase);
    }

    private void createPropsTable(SQLiteDatabase sqLiteDatabase) {
        // property value table
        sqLiteDatabase.execSQL(
                "CREATE TABLE " + PropsDataSource.TABLE_NAME     + " ( " +
                        PropsDataSource.COLUMN_ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        PropsDataSource.COLUMN_KEY              + " TEXT UNIQUE, " +
                        PropsDataSource.COLUMN_VALUE            + " TEXT " +
                        " ); "
        );
    }

    private void createMessagesTable(SQLiteDatabase sqLiteDatabase) {
        // messages table
        sqLiteDatabase.execSQL(
                "CREATE TABLE " + MessageDataSource.TABLE_NAME    + " ( " +
                        MessageDataSource.COLUMN_ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        MessageDataSource.COLUMN_KEY_REMOTE_JID   + " TEXT NOT NULL, " +
                        MessageDataSource.COLUMN_USER_JID         + " TEXT, " +
                        MessageDataSource.COLUMN_KEY_FROM_ME      + " INTEGER, " +
                        MessageDataSource.COLUMN_NEEDS_PUSH       + " INTEGER, " +
                        MessageDataSource.COLUMN_DATA             + " TEXT, " +
                        MessageDataSource.COLUMN_FILE             + " INTEGER, " +
                        MessageDataSource.COLUMN_TIMESTAMP        + " INTEGER " +
                        " );"
        );
    }

    private void createChatListTable(SQLiteDatabase sqLiteDatabase) {
        // chat list table
        sqLiteDatabase.execSQL(
                "CREATE TABLE " + ChatDataSource.TABLE_NAME      + " ( " +
                        ChatDataSource.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        ChatDataSource.COLUMN_KEY_REMOTE_JID     + " TEXT UNIQUE, " +
                        ChatDataSource.COLUMN_MESSAGE_TABLE_ID   + " INTEGER, " +
                        ChatDataSource.COLUMN_SORT_TIMESTAMP     + " INTEGER " +
                        " );"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.w(DBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MessageDataSource.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ChatDataSource.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PropsDataSource.TABLE_NAME);
        onCreate(sqLiteDatabase);

    }
}
