package com.generation_change.mhooda.locchatapp.controller.adapter;

import android.content.Context;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.activity.MainActivity;
import com.generation_change.mhooda.locchatapp.controller.activity.MessageActivity;
import com.generation_change.mhooda.locchatapp.global.GlobalDataContainer;
import com.generation_change.mhooda.locchatapp.model.Chat;
import com.generation_change.mhooda.locchatapp.model.Message;

import java.util.ArrayList;


/**
 * Created by himt18 on 12/11/15.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatHolder> {
    private ArrayList<Chat> mChats;
    private Context mContext;

    public static class ChatHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mChatNameTextView;
        private ImageView mChatIconImageView;
        private Context mContext;
        private Chat mChat;

        public ChatHolder(View itemView, Context context) {
            super(itemView);
            itemView.setOnClickListener(this);
            mChatNameTextView = (TextView) itemView.findViewById(R.id.chat_name);
            mChatIconImageView = (ImageView) itemView.findViewById(R.id.chat_icon);
            mContext = context;
        }

        public void bindChat(Chat chat) {
            mChat = chat;
            mChatNameTextView.setText(chat.getKeyRemoteJid());
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, MessageActivity.class);
            intent.putExtra(GlobalDataContainer.CHAT_BUNDLE, mChat);
            mContext.startActivity(intent);
        }
    };

    public ChatAdapter(Context context, ArrayList<Chat> chats){
        mChats = chats;
        mContext = context;
    }

    @Override
    public ChatHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(
                R.layout.chat_list_item, parent, false);
        return new ChatHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(ChatHolder holder, int position){
        Chat chat = mChats.get(position);
        holder.bindChat(chat);

    }

    @Override
    public int getItemCount(){
        return mChats.size();
    }
}

