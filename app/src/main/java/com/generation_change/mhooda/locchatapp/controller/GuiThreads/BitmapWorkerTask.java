package com.generation_change.mhooda.locchatapp.controller.GuiThreads;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.generation_change.mhooda.locchatapp.R;

import java.lang.ref.WeakReference;

/**
 * Created by manjeet on 1/1/16.
 */
public class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {

    private WeakReference<ImageView> imageViewReference;
    private int data = 0;
    public Context mContext;
    private String mFilePath;
    private Bitmap bitmap = BitmapFactory.decodeFile(mFilePath);


    public BitmapWorkerTask(ImageView imageView) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        imageViewReference = new WeakReference<ImageView>(imageView);

    }

    public void getDataFromAdapter(Context context, String FilePath){
        mContext = context;
        mFilePath = FilePath;
    }


    //bitmap = getRoundedCornerBitmap(bitmap);


    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Integer... params) {
        float targetW = mContext.getResources().getDimension(R.dimen.chat_image_width);
        float targetH = 250.0f;

        data = params[0];
        if(bitmap.getHeight() > bitmap.getWidth())
        {
            float inv_aspect_ratio = (bitmap.getHeight())/(bitmap.getWidth());
            targetH =  inv_aspect_ratio * targetW;
        }
        else
        {
            float aspect_ratio = (bitmap.getWidth())/(bitmap.getHeight());
            targetH =  targetW/aspect_ratio;

        }
        return Bitmap.createScaledBitmap(bitmap, (int) targetW, (int) targetH, true);
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            final BitmapWorkerTask bitmapWorkerTask =
                    getBitmapWorkerTask(imageView);
            if (this == bitmapWorkerTask && imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    public void loadBitmap(int resId, ImageView imageView) {
        if (cancelPotentialWork(resId, imageView)) {
            final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
            final AsyncDrawable asyncDrawable =
                    new AsyncDrawable(mContext.getResources(), bitmap, task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(resId);
        }
    }

    static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap,
                             BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    public static boolean cancelPotentialWork(int data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final int bitmapData = bitmapWorkerTask.data;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == 0 || bitmapData != data) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

}

