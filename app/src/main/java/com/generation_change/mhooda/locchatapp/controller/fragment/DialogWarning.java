package com.generation_change.mhooda.locchatapp.controller.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.generation_change.mhooda.locchatapp.R;

/**
 * Created by manjeet on 23/12/15.
 */
public class DialogWarning extends DialogFragment {
    public String Title;
    public String Message;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(Title)
                .setMessage(Message)
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }
}