package com.generation_change.mhooda.locchatapp.global;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anonymous on 12/23/15.
 */
public class JSONParser {


    private static User mUser;
    public static String uid ;
    public static User parseJSONFacebook(String response){

        mUser = new User();


        try {
            JSONObject obj = new JSONObject(response);
            mUser.setUID(obj.getString("id"));
            mUser.setName(obj.getString("name"));
            mUser.setEmail(obj.getString("email"));
            mUser.setPassword("facebook");


            return mUser ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }

}
