package com.generation_change.mhooda.locchatapp.controller.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.activity.MessageActivity;
import com.generation_change.mhooda.locchatapp.controller.activity.NewGroupActivity;
import com.generation_change.mhooda.locchatapp.global.GlobalDataContainer;
import com.generation_change.mhooda.locchatapp.model.Chat;

import java.util.ArrayList;

/**
 * Created by manjeet on 23/12/15.
 */
public class FinalCreateNewGroup extends Fragment {
    private NewGroupActivity newGroupActivity;
    private TextView create;
    private View view;
    private Chat mChat = new Chat(); //TODO : PLEASE CHECK MCHAT ..IT IS CREATED BY ME NOW

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.final_create_new_group, container, false);
        setupCreate();
        return view;
    }

    private void setupCreate() {
        create = (TextView) view.findViewById(R.id.create_new_gp_create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MessageActivity.class);
                intent.putExtra(GlobalDataContainer.CHAT_BUNDLE, mChat);
                intent.putExtra("gpPhoto","group_1");
                intent.putExtra("group","true");
                getContext().startActivity(intent);
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        newGroupActivity = (NewGroupActivity) getActivity();
        mChat.setKeyRemoteJid(newGroupActivity.getGpName());
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}

