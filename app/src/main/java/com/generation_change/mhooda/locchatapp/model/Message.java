package com.generation_change.mhooda.locchatapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by himt18 on 22/10/15.
 */
public class Message implements Parcelable {
    private int mId;
    private String mKeyRemoteJid; //not null
    private String mUserjid;
    private int mKeyFromMe; //not null
    private int mNeedsPush;
    private String mData;
    private int mTimestamp;
    private int mFile = 0;  //by default set to 0 means no file

    public int getId() {
        return mId;
    }

    public String getKeyRemoteJid() {
        return mKeyRemoteJid;
    }

    public int getKeyFromMe() {
        return mKeyFromMe;
    }

    public int getNeedsPush() {
        return mNeedsPush;
    }

    public String getData() {
        return mData;
    }

    public int getTimestamp() {
        return mTimestamp;
    }

    public String getUserJid(){ return mUserjid; }

    public int isFile(){ return mFile; }

    // id not to be set when inserting Message
    public void setId(int id) {
        this.mId = id;
    }

    public void setKeyRemoteJid(String mKeyRemoteJid) {
        this.mKeyRemoteJid = mKeyRemoteJid;
    }

    public void setUserJid(String mUserjid) { this.mUserjid = mUserjid; }

    public void setKeyFromMe(int mKeyFromMe) {
        this.mKeyFromMe = mKeyFromMe;
    }

    public void setNeedsPush(int mNeedsPush) {
        this.mNeedsPush = mNeedsPush;
    }

    public void setData(String mData) {
        this.mData = mData;
    }

    public void setFile(int file) { this.mFile = file; }

    public void setTimestamp(int mTimestamp) {
        this.mTimestamp = mTimestamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(mId);
        out.writeString(mKeyRemoteJid);
        out.writeString(mUserjid);
        out.writeInt(mKeyFromMe);
        out.writeInt(mNeedsPush);
        out.writeString(mData);
        out.writeInt(mFile);
        out.writeInt(mTimestamp);

    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel parcel) {
            return new Message(parcel);
        }

        @Override
        public Message[] newArray(int i) {
            return new Message[i];
        }
    };


    private Message(Parcel in) {
        mId = in.readInt();
        mKeyRemoteJid = in.readString();
        mKeyFromMe = in.readInt();
        mNeedsPush = in.readInt();
        mData = in.readString();
        mFile = in.readInt();
        mTimestamp = in.readInt();

    }

    public Message() {}


}
