package com.generation_change.mhooda.locchatapp.controller.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.generation_change.mhooda.locchatapp.R;
import com.generation_change.mhooda.locchatapp.controller.activity.MainActivity;
import com.generation_change.mhooda.locchatapp.global.AppSession;
import com.generation_change.mhooda.locchatapp.global.User;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;


/**
 * Created by anonymous on 12/8/15.
 */
public class GPlusFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "GPlusFragent";
    private int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private SignInButton signInButton;
    private Button signOutButton;
    private Button disconnectButton;
    private LinearLayout signOutView;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;
    private ImageView imgProfilePic;
    private AppSession session;
    private User mGoogleUser;




    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        session = AppSession.get(getActivity());
        mGoogleUser = new User();
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
// options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();


    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gplus, parent, false);

        signInButton = (SignInButton) v.findViewById(R.id.sign_in_button);
//        signOutButton = (Button) v.findViewById(R.id.sign_out_button);
//        disconnectButton = (Button) v.findViewById(R.id.disconnect_button);
//        imgProfilePic = (ImageView) v.findViewById(R.id.img_profile_pic);
//
//        signOutView = (LinearLayout) v.findViewById(R.id.sign_out_and_disconnect);
//        mStatusTextView = (TextView) v.findViewById(R.id.status);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }

        });


//        signOutButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//                                updateUI(false);
//                            }
//                        });
//            }
//
//        });

//        disconnectButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//                                // [START_EXCLUDE]
//                                updateUI(false);
//                                // [END_EXCLUDE]
//                            }
//                        });            }
//
//        });

        return v;
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());









        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            mGoogleUser.setName(acct.getDisplayName());
            mGoogleUser.setEmail(acct.getEmail());
            mGoogleUser.setPassword("google");

            if(acct.getPhotoUrl() !=null) {

                mGoogleUser.setPhotoURL(acct.getPhotoUrl().toString());

            }


            session.createLoginSession(mGoogleUser.getName(), mGoogleUser.getEmail(), mGoogleUser.getPassword(), mGoogleUser.getPhotoURL());

            Toast t = Toast.makeText(getActivity(),"Name: "+mGoogleUser.getName() +"Email: "+mGoogleUser.getEmail()+"Photo: "+mGoogleUser.getPhotoURL(),Toast.LENGTH_LONG);
            t.show();
            Intent i = new Intent(getActivity(),MainActivity.class);
            startActivity(i);





//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()+ acct.getEmail() + acct.getPhotoUrl()));
//            new LoadProfileImage(imgProfilePic).execute(acct.getPhotoUrl().toString());
//




//            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);

            Toast t = Toast.makeText(getActivity(),"Name:",Toast.LENGTH_LONG);
            t.show();

        }
    }




//    private void updateUI(boolean signedIn) {
//        if (signedIn) {
//            signInButton.setVisibility(View.GONE);
//            signOutView.setVisibility(View.VISIBLE);
//        } else {
//            mStatusTextView.setText(R.string.signed_out);
//
//            signInButton.setVisibility(View.VISIBLE);
//            signOutView.setVisibility(View.GONE);
//        }
//    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();


        }

    }


    /**
     * Background Async task to load user profile picture from url
     * */
//    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
//        ImageView bmImage;
//
//        public LoadProfileImage(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//        protected Bitmap doInBackground(String... uri) {
//            String url = uri[0];
//            Bitmap mIcon11 = null;
//            try {
//                InputStream in = new URL(url).openStream();
//                mIcon11 = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            return mIcon11;
//        }

//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(ImageHelper.getRoundedCornerBitmap(getContext(), result, 250, 200, 200, false, false, false, false));
//        }
//    }


}
