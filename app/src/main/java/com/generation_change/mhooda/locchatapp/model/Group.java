package com.generation_change.mhooda.locchatapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.generation_change.mhooda.locchatapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manjeet on 5/12/15.
 */
public class Group implements Parcelable {
    private int mPhotoid;
    private String mGpName;
    private String mNumMember;
    private String mDescription;


    // id not to be set when inserting or updating chat
    public Group setmPhotoId(int id) {
        this.mPhotoid = id;
        return this;
    }

    public int getmPhotoid(){
        return mPhotoid;
    }

    public String getmGpName() {
        return mGpName;
    }

    public Group setmGpName(String mGpName) {
        this.mGpName = mGpName;
        return this;
    }

    public String getmDescription() {
        return mDescription;
    }

    public Group setmDescription(String mDescription) {
        this.mDescription = mDescription;
        return this;
    }

    public String getmGpNumMem() {
        return mNumMember;
    }

    public Group setmGpNumMem(String mGpNumMem) {
        this.mNumMember = mGpNumMem;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(mGpName);
        out.writeString(mNumMember);
        out.writeInt(mPhotoid);
        out.writeString(mDescription);
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel parcel) {
            return new Group(parcel);
        }

        @Override
        public Group[] newArray(int i) {
            return new Group[i];
        }
    };

    private Group (Parcel in) {
        mGpName = in.readString();
        mNumMember = in.readString();
        mPhotoid = in.readInt();
        mDescription = in.readString();
    }

    public Group () {}
}