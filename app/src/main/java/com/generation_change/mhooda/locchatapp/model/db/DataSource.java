package com.generation_change.mhooda.locchatapp.model.db;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by himt18 on 08/11/15.
 */

public abstract class DataSource<T> {
    protected SQLiteDatabase mSQLiteDatabase;

    public DataSource(SQLiteDatabase sqLiteDatabase) {
        mSQLiteDatabase = sqLiteDatabase;
    }

    // CRUD
    public abstract boolean insert(T entity);
    public abstract ArrayList<T> read();
    public abstract ArrayList read(String selection, String[] selectionArgs,
                              String groupBy, String having, String orderBy);
    public abstract boolean update(T entity);
    public abstract boolean delete(T entity);

}
